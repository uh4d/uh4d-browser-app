# UH4D Browser App

This is the code base of the main frontend application of the _UrbanHistory4D_ project.

Live demo: https://4dbrowser.urbanhistory4d.org

More information about the project: www.urbanhistory4d.org

## Production

Follow [detailed instructions](server-setup.md) on how to set up and serve the UH4D backend and frontend for production purpose.

## Development

Code documentation (still growing...): https://4dbrowser.urbanhistory4d.org/docs/

More information coming soon...

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
