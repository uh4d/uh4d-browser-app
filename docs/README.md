# Generating Docs

[Compodoc](https://compodoc.app/) is used to generate documentation pages for the code of the UH4D Browser App.

Run following npm command to generate the docs in `dist/docs`.

    npm run docs

Deployed documentation: https://4dbrowser.urbanhistory4d.org/docs/

The documentation is directly done within the code at respective classes, methods, and properties.
This directory only contains assets like referenced figures.
