/**
 * @param grunt {IGrunt}
 */
module.exports = function (grunt) {

  // load plugins
  grunt.loadNpmTasks('grunt-version');

  // configuration
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    version: {
      html: {
        options: {
          prefix: 'UH4D\\sBrowser\\sv'
        },
        src: ['src/app/shared/components/footer/footer.component.html']
      },
      packages: {
        src: ['package.json']
      }
    }
  });

};
