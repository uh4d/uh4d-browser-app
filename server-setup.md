# Setup on Ubuntu Server

This guide describes the necessary steps on how to set up and run the [UH4D Backend](https://gitlab.com/uh4d/uh4d-backend) and serve the frontend.
The UH4D Browser App depends on several tools and applications.

In this guide, existing data will be copied.
Setting up an empty UH4D instance is not yet possible.

## System requirements

Virtual server with Linux (Ubuntu)

Following requirements are based on experiences:

| Minimum | Recommended |
| --- | --- |
| 2 CPU vCore | 4 CPU vCore |
| 4 GB RAM | 8 GB RAM |

__Disk space:__ Currently, there are 8 GB of application data.
Hence, at least 30 GB (better 50 GB) disk space are recommended.
If the database is going to be extended, disk space should be much more.

## Database

All information is stored in the graph database [Neo4j](https://neo4j.com/).
Any files are stored within the file system and are referenced in the database.

### Java 11 (OpenJDK)

Neo4j needs Java 11 environment to run.
Add the official OpenJDK package repository to `apt`:

```bash
$ sudo add-apt-repository -y ppa:openjdk-r/ppa
$ sudo apt-get update
```

The next commands will install Java 11 automatically if it is not already installed.

```bash
$ sudo apt-get install openjdk-11-jdk
```

### Neo4j

Minimum version `4.1.0` is required.

```bash
$ wget -O - https://debian.neo4j.com/neotechnology.gpg.key | sudo apt-key add -
$ echo 'deb https://debian.neo4j.com stable 4.1' | sudo tee -a /etc/apt/sources.list.d/neo4j.list
$ sudo apt-get update
```

There are two versions of Neo4j: _Community Edition_ and _Enterprise Edition_.
The major difference is that the Enterprise Edition can run multiple graphs.
If UH4D is your only application using Neo4j, the Community Edition should sufficient.

Either install Community Edition:

```bash
$ sudo apt-get install neo4j=1:4.1.6
```

or Enterprise Edition:

```bash
$ sudo apt-get install neo4j-enterprise=1:4.1.6
```

More detailed instructions: https://neo4j.com/docs/operations-manual/4.1/installation/linux/debian/

### Neo4j APOC Procedures

The APOC library adds many useful procedures and functions to help with different tasks and are used in several cypher queries. 

APOC is downloaded and directly copied into the `plugins` folder of the Neo4j instance.

```bash
$ wget https://github.com/neo4j-contrib/neo4j-apoc-procedures/releases/download/4.1.0.8/apoc-4.1.0.8-all.jar -P /var/lib/neo4j/plugins/
```

__Note:__ Check the Neo4j directories, such that it will be downloaded into the right folder (see [file locations](https://neo4j.com/docs/operations-manual/4.1/configuration/file-locations/#file-locations)).

Also check [version compatibilities](https://github.com/neo4j-contrib/neo4j-apoc-procedures#version-compatibility-matrix).

More detailed instructions: https://neo4j.com/labs/apoc/4.1/installation/

### Configure and run Neo4j

#### Admin password

Set an initial password for the Neo4j instance:

```bash
$ neo4j-admin set-initial-password <password>
```

#### System Service

Enable the service to automatically start Neo4j on system start.

```bash
$ systemctl enable neo4j
$ systemctl {start|stop|restart} neo4j
```

More information: https://neo4j.com/docs/operations-manual/4.1/installation/linux/systemd/

When starting Neo4j, it may be that you will be prompted with a warning regarding the number of open files.
In this case, you can edit the service configuration:

```bash
$ systemctl edit neo4j
```

Append the following lines:

    [Service]
    LimitNOFILE=60000

and restart Neo4j:

```bash
$ systemctl restart neo4j
```

More information: https://neo4j.com/developer/kb/number-of-open-files-on-linux/

## Tools

Several tools are required as runtime environment and to set up the frontend and backend (i.e. building the application).
Some software packages are required for special operations such as image and 3D model processing.

### Curl

Curl is used to install Node.js.

```bash
$ sudo apt install curl
```

### Node.js

[Node.js](https://nodejs.org/en/) is the basic runtime environment for the backend application, but is also used to build the frontend application.
Minimum version `16.7.x` is required.

```bash
$ curl -fsSL https://deb.nodesource.com/setup_16.x | sudo -E bash -
$ sudo apt-get install -y nodejs
```

More information: https://github.com/nodesource/distributions/blob/master/README.md#debinstall

### PM2

PM2 is a production process manager for Node.js applications.
It will start scripts after system start.

```bash
$ npm install -g pm2
```

See [Quick start and cheatsheet](https://pm2.keymetrics.io/docs/usage/quick-start/#cheatsheet)

To prepare PM2 for startup, run following command and follow the instructions:

```bash
$ pm2 startup
```

See [Startup script generator](https://pm2.keymetrics.io/docs/usage/startup/)

### Angular CLI

The Angular command line interface is needed to build the frontend application.

```bash
$ npm install -g @angular/cli
```

### Git

[Git](https://git-scm.com/) is a version control system and used to clone the code from the repositories.

```bash
$ sudo apt install git
```

### CMake

A few tools need to be compiled from source.
For that, we need C/C++ compilers.

```bash
$ sudo apt install build-essential
```

[CMake](https://cmake.org/) ist used to control the software compilation process.

```bash
$ sudo apt-get install cmake
```

### Assimp

[Assimp](https://github.com/assimp/assimp) is used in the 3D model conversion process.
It converts `.obj` files to `.dae` and runs some optimization steps.

Clone and build project:

```bash
$ git clone https://github.com/assimp/assimp.git

$ cd assimp

$ cmake CMakeLists.txt

$ cmake --build .
```

The generated executable are located in `./bin/assimp`

### COLLADA2GLTF

[COLLADA2GLTF](https://github.com/KhronosGroup/COLLADA2GLTF) is used in 3D model conversion process.
It converts `.dae` files to `.gltf` including DRACO compression.

The latest version `v2.1.5` seems to have a bug: if the flag is set to enable DRACO compression, no `.gltf` is generated.
For this reason, `v2.1.4` is used.

Clone version `v2.1.4`. `--recursive` flag automatically clones submodules.

```bash
$ git clone --depth 1 --branch v2.1.4 --recursive https://github.com/KhronosGroup/COLLADA2GLTF.git
```

Build project:

```bash
$ cd COLLADA2GLTF
$ mkdir build
$ cd build
$ cmake ..

$ make
```

The generated executable are located in `./COLLADA2GLTF-bin`

## Copy data

Copy existing data

```bash
$ wget https://4dbrowser.urbanhistory4d.org/data/uh4d-data.zip
$ wget https://4dbrowser.urbanhistory4d.org/data/uh4d-all.cypher
```

Since `unzip` has problems with big zip files, we use `7z` to unpack the data.

```bash
$ sudo apt-get install p7zip p7zip-full p7zip-rar
$ 7z x -ouh4d-data uh4d-data.zip
```

The files are now extracted into the folder named `uh4d-data`.

Import the database dump into Neo4j database: 

```bash
$ cypher-shell -u neo4j -p <password> -d neo4j -f uh4d-all.cypher
```

__Note:__ `neo4j` is the default user and database name.
If you set up something else, you may need to update the above command line appropriately.

## uh4d-backend

Clone the project, install the dependencies, and build the project:

```bash
$ git clone https://gitlab.com/uh4d/uh4d-backend.git

$ cd uh4d-backend

$ npm install

$ npm run build
```

Copy the `config-template.yaml` and configure the settings according to how you set up the credentials for Neo4j, where you copied the data, and where the executables are located.

```bash
$ cp config-template.yaml config.yaml
$ vi config.js
```

```yaml
host: https://your-domain.com
port: 3001

neo4j:
  scheme: neo4j
  host: localhost
  port: 7687
  username: neo4j
  password: password
  database: neo4j

path:
  data: 'path/to/data'
  logs: 'path/to/logs'
  cache: 'path/to/cache'

exec:
  # assimp cli command
  Assimp: 'assimp'
  # collada2gltf cli command
  Collada2Gltf: 'COLLADA2GLTF-bin'

# only for bug reports
gitlab:
  uh4d-browser-app:
    apiEndpoint: https://gitlab.com/api/v4
    projectId: '18567666'
    privateToken: '<your-access-token>'
  vrcity-app:
    apiEndpoint: https://git.uni-jena.de/api/v4
    projectId: '396'
    privateToken: '<your-access-token>'
```

Ensure that the directories for `data`, `logs`, `cache` exist.
`data` should be the path to which we extracted the zip file (`uh4d-data`).
`logs` and `cache` can be (but not have to be) folders inside `data` and need to be created manually.

Start the script using PM2:

```bash
$ pm2 start npm --name uh4d -- run start:prod
$ pm2 save
```

## uh4d-browser-app

Depending on how the application is served (see [Web server](#web-server)), the files need to be cloned into the right vhost directory.

Clone the project and install the dependencies:

```bash
$ git clone https://gitlab.com/uh4d/uh4d-browser-app.git

$ cd uh4d-browser-app

$ npm install
```

Build the application:

```bash
$ npm run build
```

The generated files are located at `dist/uh4d-browser-app` and can now be served by the web server.

## Web server

A web server is needed to serve the website and the backend.
The two most common web server are [Apache](https://www.apache.org/) and [nginx](https://www.nginx.com/).
Read respective documentation on how to install the software and how to serve a website.
These steps may be easier to perform with a server management platform, e.g. [Plesk](https://www.plesk.com/).

<details>
<summary>Apache config example</summary>

Enable modules <code>http2</code>, <code>proxy_http</code>, <code>proxy_http2</code>.

Important pieces of the config (as it is generated by Plesk):
<pre><code>&lt;IfModule mod_ssl.c&gt;

    &lt;VirtualHost 81.169.153.72:7081 &gt;
        ServerName "4dbrowser.urbanhistory4d.org"
        ServerAlias "www.4dbrowser.urbanhistory4d.org"
        ServerAlias "ipv4.4dbrowser.urbanhistory4d.org"
        UseCanonicalName Off

        DocumentRoot "/var/www/vhosts/urbanhistory4d.org/4dbrowser.urbanhistory4d.org"
  
        SSLEngine on
        SSLVerifyClient none
        SSLCertificateFile /opt/psa/var/certificates/scfLXihrI
        SSLCACertificateFile /opt/psa/var/certificates/scf3NytaF

        &lt;Directory /var/www/vhosts/urbanhistory4d.org/4dbrowser.urbanhistory4d.org&gt;
  
            &lt;IfModule mod_fcgid.c&gt;
                &lt;Files ~ (\.fcgi$)&gt;
                    SetHandler fcgid-script
                    Options +ExecCGI
                &lt;/Files>
            &lt;/IfModule>
            &lt;IfModule mod_proxy_fcgi.c&gt;
                &lt;Files ~ (\.php$)&gt;
                    SetHandler proxy:unix:///var/www/vhosts/system/4dbrowser.urbanhistory4d.org/php-fpm.sock|fcgi://127.0.0.1:9000
                &lt;/Files&gt;
            &lt;/IfModule&gt;

            SSLRequireSSL

            Options -Includes -ExecCGI

        &lt;/Directory&gt;

        &lt;Directory /var/www/vhosts/urbanhistory4d.org&gt;
                Options +FollowSymLinks
        &lt;/Directory&gt;

        DirectoryIndex "index.html" "index.cgi" "index.pl" "index.php" "index.xhtml" "index.htm" "index.shtml"

        &lt;Directory /var/www/vhosts/urbanhistory4d.org&gt;
            AllowOverride AuthConfig FileInfo Indexes Limit Options=Indexes,SymLinksIfOwnerMatch,MultiViews,FollowSymLinks,ExecCGI,Includes,IncludesNOEXEC
        &lt;/Directory&gt;
               
    &lt;/VirtualHost&gt;

&lt;/IfModule&gt;

&lt;VirtualHost 81.169.153.72:7080 &gt;
    ServerName "4dbrowser.urbanhistory4d.org"
    ServerAlias "www.4dbrowser.urbanhistory4d.org"
    ServerAlias "ipv4.4dbrowser.urbanhistory4d.org"
    UseCanonicalName Off

    &lt;IfModule mod_rewrite.c&gt;
        RewriteEngine On
        RewriteCond %{HTTPS} off
        RewriteRule ^ https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L,QSA]
    &lt;/IfModule&gt;
&lt;/VirtualHost&gt;</code></pre>
</details>

<details>
<summary>nginx config example</summary>

Important pieces of the config (as it is generated by Plesk):
<pre><code>server {
    listen 81.169.153.72:443 ssl http2;

    server_name 4dbrowser.urbanhistory4d.org;
    server_name www.4dbrowser.urbanhistory4d.org;
    server_name ipv4.4dbrowser.urbanhistory4d.org;

    ssl_certificate             /opt/psa/var/certificates/scfLXihrI;
    ssl_certificate_key         /opt/psa/var/certificates/scfLXihrI;

    client_max_body_size 128m;

    root "/var/www/vhosts/urbanhistory4d.org/4dbrowser.urbanhistory4d.org";
    access_log "/var/www/vhosts/system/4dbrowser.urbanhistory4d.org/logs/proxy_access_ssl_log";
    error_log "/var/www/vhosts/system/4dbrowser.urbanhistory4d.org/logs/proxy_error_log";

    location / {
        proxy_pass https://81.169.153.72:7081;
        proxy_set_header Host             $host;
        proxy_set_header X-Real-IP        $remote_addr;
        proxy_set_header X-Forwarded-For  $proxy_add_x_forwarded_for;
        proxy_set_header X-Accel-Internal /internal-nginx-static-location;
        access_log off;

    }

    location /internal-nginx-static-location/ {
        alias /var/www/vhosts/urbanhistory4d.org/4dbrowser.urbanhistory4d.org/;
        internal;
    }
}

server {
    listen 81.169.153.72:80;

    server_name 4dbrowser.urbanhistory4d.org;
    server_name www.4dbrowser.urbanhistory4d.org;
    server_name ipv4.4dbrowser.urbanhistory4d.org;

    client_max_body_size 128m;

    location / {
        return 301 https://$host$request_uri;
    }
}</code></pre>
</details>

__Note:__ Ensure that the document root points to the right directory.

### Reverse proxy

The basic website should now be served.
But to access the backend application and the data, we need to set up a reverse proxy pass.

Add these additional instructions depending on your type of web server.

__Apache:__

```
ProxyPass /api http://localhost:3001/api
ProxyPassReverse /api http://localhost:3001/api
ProxyPass /data/ http://localhost:3001/data/
ProxyPassReverse /data/ http://localhost:3001/data/
ProxyPass /logs ws://localhost:3001/logs
```

__nginx:__

```
location /api {
    proxy_pass http://localhost:3001/api;
}
location /data {
    proxy_pass http://localhost:3001/data;
}
location /logs {
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "upgrade";
    proxy_pass http://localhost:3001/logs;
}

gzip on;
gzip_types text/css application/javascript application/json;
```

### Secure context

Some features are restricted to secure contexts, such as HTTP/2 (faster content loading) or geolocation (on mobile devices).
For secure HTTP connections (HTTPS), SSL certificates need to be installed.

Free SSL certificates can be obtained with [Let's Encrypt](https://letsencrypt.org).
Depending on the used web server, these certificates need to be installed differently.
If using Plesk, this step is rather simple utilizing respective integrations.
