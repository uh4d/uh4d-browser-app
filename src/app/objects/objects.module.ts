import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { ObjectSheetComponent } from './components/object-sheet/object-sheet.component';
import { ObjectViewerComponent } from './components/object-viewer/object-viewer.component';
import { TextsModule } from '../texts/texts.module';

/**
 * Module with resolver and components related to 3D objects representing buildings.
 */
@NgModule({
  declarations: [
    ObjectSheetComponent,
    ObjectViewerComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TextsModule
  ]
})
export class ObjectsModule { }
