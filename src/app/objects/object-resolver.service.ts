import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';
import { ObjectService } from '@data/services/object.service';
import { IObject } from '@data/schema/object';

/**
 * Resolver service invoked when entering {@link ObjectSheetComponent}.
 */
@Injectable({
  providedIn: 'root'
})
export class ObjectResolverService implements Resolve<IObject> {

  constructor(
    private router: Router,
    private objectService: ObjectService
  ) { }

  /**
   * When entering the route, query database for object entry with given `objectId` parameter.
   * If no entry found, navigate back to home page.
   */
  resolve(route: ActivatedRouteSnapshot): Observable<IObject> | Promise<IObject> | IObject {

    return this.objectService.get(route.paramMap.get('objectId'))
      .pipe(
        take(1),
        mergeMap(object => {
          if (object.id) {
            return of(object);
          } else {
            // not found
            this.router.navigate(['/']);
            return EMPTY;
          }
        })
      );

  }

}
