import { Component, HostListener, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom, Observable, of, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { RestElement } from '@brakebein/ngx-restangular';
import * as moment from 'moment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { NotificationsService } from 'angular2-notifications';
import { NgxPermissionsService } from 'ngx-permissions';
import { AddressService } from '@data/services/address.service';
import { TagService } from '@data/services/tag.service';
import { ObjectService } from '@data/services/object.service';
import { IObject } from '@data/schema/object';
import { ConfirmModalService } from '@shared/services/confirm-modal.service';
import { SearchManagerService } from '@shared/services/search-manager.service';
import { ViewportEventsService } from '../../../viewport3d/services/viewport-events.service';
import { Cache } from '../../../viewport3d/viewport3d-cache';
import { ObjectViewerComponent } from '../object-viewer/object-viewer.component';
import { TextUploadModalComponent } from '../../../texts/components/text-upload-modal/text-upload-modal.component';

/**
 * Component that shows all the metadata of an object.
 * In edit mode, the metadata can be updated.
 * The object as an 3D model is displayed within an interactive object viewer.
 *
 * ![Screenshot](../assets/object-sheet.png)
 */
@Component({
  selector: 'uh4d-object-sheet',
  templateUrl: './object-sheet.component.html',
  styleUrls: ['./object-sheet.component.sass']
})
export class ObjectSheetComponent implements OnInit, OnDestroy {

  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Object database entry.
   */
  object: RestElement<IObject>;
  /**
   * Flag that if `true` will unhide buttons to edit metadata.
   */
  canEditObjects = false;
  /**
   * Flag that if `true` will unhide some buttons with more crucial operations.
   */
  expertMode = false;

  /**
   * Reference to the object viewer component.
   */
  @ViewChild(ObjectViewerComponent) objectViewer: ObjectViewerComponent;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private notify: NotificationsService,
    private objectService: ObjectService,
    private addressService: AddressService,
    private tagService: TagService,
    private searchManager: SearchManagerService,
    private confirmService: ConfirmModalService,
    private permissionsService: NgxPermissionsService,
    private viewportEvents: ViewportEventsService,
    private modalService: BsModalService
  ) {

    this.queryAddresses = this.queryAddresses.bind(this);
    this.queryTags = this.queryTags.bind(this);

  }

  /**
   * Invoked after the component is instantiated.
   * Get object data from resolver and listen to permission updates.
   */
  ngOnInit(): void {

    this.route.data.subscribe(data => {
      this.object = data.object;
      console.log(this.object);
    });

    this.permissionsService.permissions$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(permissions => {
        this.canEditObjects = !!permissions.editObjects;
      });

  }

  /**
   * Return to 3D viewport and jump to object that it fits the viewport.
   */
  zoomObject(): void {

    const item = Cache.objects.getByName(this.object.id);

    if (item) {
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve'
      });
      item.focus();
    }

  }

  /**
   * Update metadata of the object. Only one property can be updated at once.
   * @param prop Name of property that should be updated
   */
  updateObject(prop: string): void {

    switch (prop) {
      case 'name':
        if (!this.object.name) {
          this.notify.error('Object must have a name!');
          return;
        }
        break;
      case 'from':
        if (this.object.date.from) {
          const fromDate = moment(this.object.date.from);
          if (this.object.date.to && moment(this.object.date.to).isBefore(fromDate)) {
            this.notify.error('Date must be before deconstruction date!');
            return;
          }
        }
        prop = 'date';
        break;
      case 'to':
        if (this.object.date.to) {
          const toDate = moment(this.object.date.to);
          if (this.object.date.from && moment(this.object.date.from).isAfter(toDate)) {
            this.notify.error('Date must be after erection date!');
            return;
          }
        }
        prop = 'date';
        break;
      case 'tags':
        console.log(this.object.tags);
    }

    this.object.patch({ [prop]: this.object[prop] })
      .subscribe(result => {
        console.log(result);
      });

  }

  /**
   * Method passed to typeahead to query addresses.
   * @param input Typed input
   */
  queryAddresses(input: string): Observable<string[]> {

    if (input) {
      return this.addressService.query(input)
        .pipe(
          map((results) => results.plain().map(r => r.value))
        );
    }

    return of([]);

  }

  /**
   * Method passed to typeahead to query tags.
   * @param input Typed input
   */
  queryTags(input: string): Observable<string[]> {

    if (input) {
      return this.tagService.query(input)
        .pipe(
          map((results) => results.plain().map(r => r.value))
        );
    }

    return of([]);

  }

  /**
   * Open modal to upload a new associated text.
   */
  openTextUploadModal(): void {
    const modalRef = this.modalService.show(TextUploadModalComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true
    });
    modalRef.content.objectIds = [this.object.id];
  }

  /**
   * Toggle {@link #expertMode expertMode} when user presses <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>D</kbd>.
   * @private
   */
  @HostListener('document:keyup.shift.alt.d')
  private toggleExpertMode(): void {

    this.expertMode = !this.expertMode;

  }

  /**
   * Return to 3D viewport and enter object transformation mode.
   */
  updateTransform(): void {

    this.router.navigate(['../..'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve'
    });

    this.viewportEvents.callAction$.next({ key: 'transform-update', object: this.object });

  }

  /**
   * Duplicate the database entry (reference to 3D model files will remain the same).
   * The user has to confirm this operation.
   * If successful, enter object sheet of duplicate.
   */
  async duplicateObject(): Promise<void> {

    const confirmed = await this.confirmService.confirm({
      message: 'This operation will duplicate the database entry, such that metadata can be altered. The 3D model remains the same. Do you want to continue?'
    });

    if (!confirmed) { return; }

    this.objectService.duplicate(this.object)
      .subscribe(result => {
        this.router.navigate(['..', result.id], {
          relativeTo: this.route,
          queryParamsHandling: 'preserve'
        });
      });

  }

  /**
   * Remove object from database.
   * The user has to confirm this operation.
   * After successful removal, return to 3D viewport.
   */
  async removeObject(): Promise<void> {

    const confirmed = await this.confirmService.confirm({
      message: 'This operation will remove the object from database and delete all corresponding files. Do you want to continue?'
    });

    if (!confirmed) { return; }

    this.object.remove()
      .subscribe(() => {
        this.searchManager.queryModels();
        this.router.navigate(['../..'], {
          relativeTo: this.route,
          queryParamsHandling: 'preserve'
        });
      });

  }

  downloadObject(): void {

    // get obj data
    const objData = this.objectViewer.exportObjectAsOBJ();

    // create mock a element and trigger click event
    const a = document.createElement('a');
    a.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(objData));
    a.setAttribute('download', this.object.name + '.obj');
    a.click();

  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {

    this.unsubscribe$.next();
    this.unsubscribe$.complete();

  }

}
