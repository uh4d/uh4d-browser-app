import { Component, ElementRef, HostListener, Input, NgZone, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, fromEvent, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  AmbientLight,
  Box3, DirectionalLight, Line, LinearEncoding,
  LineSegments, MathUtils, Matrix4,
  Mesh, MeshStandardMaterial,
  Object3D,
  PerspectiveCamera, Points,
  Scene,
  Sphere, Vector3,
  WebGLRenderer
} from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { OBJExporter } from 'three/examples/jsm/exporters/OBJExporter';
import { Cache } from '../../../viewport3d/viewport3d-cache';
import { CoordConverterService } from '../../../viewport3d/services/coord-converter.service';
import { ILocationOrientation } from '@data/schema/location';

/**
 * Component to view and inspect a single 3D object loaded from a glTF file.
 *
 * ![Screenshot](../assets/object-viewer.png)
 */
@Component({
  selector: 'uh4d-object-viewer',
  template: '<canvas #canvasElement></canvas>',
  styleUrls: ['./object-viewer.component.sass']
})
export class ObjectViewerComponent implements OnInit, OnDestroy {

  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Observable as part to listen to `path` input binding updates.
   * @private
   */
  private path$ = new BehaviorSubject<string>('');

  /**
   * Reference to the `canvas` element.
   */
  @ViewChild('canvasElement', { static: true }) canvasElement: ElementRef;

  /**
   * Input binding to set the path to the glTF file.
   * If the value changes, existing object will be disposed and the new glTF file will be loaded.
   * @param value Path to glTF file
   */
  @Input()
  set path(value: string) {
    this.path$.next(value);
  }

  /**
   * Input binding to set the position, rotation, and scale of the 3D object.
   */
  @Input() origin: ILocationOrientation & { scale?: number | number[] } | null = null;

  /**
   * Width of the component/canvas.
   * @private
   */
  private screenWidth: number;
  /**
   * Height of the component/canvas.
   * @private
   */
  private screenHeight: number;

  private scene: Scene;
  private renderer: WebGLRenderer;
  private camera: PerspectiveCamera;
  private controls: OrbitControls;
  private directionalLight: DirectionalLight;

  /**
   * 3D object instance loaded from the glTF file.
   * @private
   */
  private object: Object3D;

  constructor(
    private element: ElementRef,
    private ngZone: NgZone,
    private coordConverter: CoordConverterService
  ) { }

  /**
   * Invoked after the component is instantiated.
   * It will initialize the 3D scene (renderer, camera, controls, etc.)
   * and listen to `path` input binding updates.
   */
  ngOnInit(): void {

    this.resizeViewer();

    this.scene = new Scene();
    this.scene.add(new AmbientLight(0x888888));
    this.directionalLight = new DirectionalLight(0xffffff, 0.7);
    this.scene.add(this.directionalLight);

    this.renderer = new WebGLRenderer({
      antialias: true,
      alpha: true,
      canvas: this.canvasElement.nativeElement
    });
    this.renderer.setClearColor(0xffffff, 0.0);

    this.camera = new PerspectiveCamera(35, this.screenWidth / this.screenHeight, 1, 2000);
    this.camera.position.set(0, 1, 2);

    this.ngZone.runOutsideAngular(() => {
      this.controls = new OrbitControls(this.camera, this.canvasElement.nativeElement);
      fromEvent(this.controls, 'change')
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(() => { this.render(); });
    });

    this.path$.pipe(takeUntil(this.unsubscribe$))
      .subscribe(path => {
        this.loadObject(path);
      });

  }

  /**
   * Render the scene (invoked after controls change).
   * @private
   */
  private render(): void {

    this.directionalLight.position.set(4, 4, 4);
    const lightMatrix = new Matrix4().makeRotationFromQuaternion(this.camera.quaternion);
    this.directionalLight.position.applyMatrix4(lightMatrix);

    this.renderer.render(this.scene, this.camera);

  }

  /**
   * Load a glTF file, add it to the scene, and set camera position to fit the viewport.
   * @param path Path to glTF file
   * @private
   */
  private loadObject(path: string): void {

    if (!['gltf'].includes(path.split('.').pop())) { return; }

    this.disposeObject();

    Cache.gltfLoader.load(path, gltf => {

      this.object = gltf.scene.children[0];

      if (this.origin) {
        this.object.position.copy(
          this.coordConverter.fromLatLon(this.origin.latitude, this.origin.longitude, this.origin.altitude)
        );
        this.object.rotation.set(this.origin.omega, this.origin.phi, this.origin.kappa, 'YXZ');
        if (this.origin.scale) {
          if (Array.isArray(this.origin.scale)) {
            this.object.scale.fromArray(this.origin.scale);
          } else {
            this.object.scale.set(this.origin.scale, this.origin.scale, this.origin.scale);
          }
        }
      }

      this.scene.add(this.object);

      this.object.traverse(obj => {
        if (obj instanceof Mesh) {
          (Array.isArray(obj.material) ? obj.material : [obj.material]).forEach(mat => {
            if ((mat as MeshStandardMaterial).map) {
              (mat as MeshStandardMaterial).map.encoding = LinearEncoding;
            }
          });
        } else if (obj instanceof LineSegments) {
          // @ts-ignore
          obj.material.dispose();
          obj.material = Cache.materials.get('edgesMat');
        }
      });

      // compute camera position to fit the viewport
      const boundingBox = new Box3();
      boundingBox.expandByObject(this.object);
      const sphere = boundingBox.getBoundingSphere(new Sphere());

      const s = new Vector3().subVectors(this.camera.position, this.controls.target);
      const h = sphere.radius / Math.tan(this.camera.fov / 2 * MathUtils.DEG2RAD);
      const pos = new Vector3().addVectors(sphere.center, s.setLength(h));

      this.camera.position.copy(pos);
      this.controls.target.copy(sphere.center);

      this.resizeViewer();

    }, null, xhr => {
      console.error('Couldn\'t load gltf file', xhr);
    });

  }

  /**
   * Remove the object from the scene and dispose geometry and materials.
   * @private
   */
  private disposeObject(): void {

    if (!this.object) { return; }

    this.scene.remove(this.object);

    this.object.traverse(obj => {
      if (obj instanceof Mesh || obj instanceof LineSegments) {
        obj.geometry.dispose();
        (Array.isArray(obj.material) ? obj.material : [obj.material]).forEach(mat => {
          Cache.materials.disposeMaterial(mat);
        });
      }
    });

    this.object = null;

  }

  /**
   * Export 3D object as OBJ file.
   * Only meshes are exported.
   */
  exportObjectAsOBJ(): string {

    // clone object and keep only meshes
    const objClone = this.object.clone(true);
    const discardObjs: Object3D[] = [];

    objClone.traverse(child => {
      if ((child as Line).isLine || (child as Points).isPoints) {
        discardObjs.push(child);
      }
    });

    for (const child of discardObjs) {
      const parent = child.parent;
      if (parent) { parent.remove(child); }
    }

    // parse object to OBJ string
    const exporter = new OBJExporter();
    return exporter.parse(objClone);

  }

  /**
   * Update renderer size and camera projection matrix (invoked at [ngOnInit](#ngOnInit) and `window:resize` event).
   * @private
   */
  @HostListener('window:resize')
  private resizeViewer(): void {

    this.screenWidth = this.element.nativeElement.offsetWidth;
    this.screenHeight = this.element.nativeElement.offsetHeight;

    if (this.camera) {
      this.camera.aspect = this.screenWidth / this.screenHeight;
      this.camera.updateProjectionMatrix();
    }

    if (this.renderer) {
      this.renderer.setSize(this.screenWidth, this.screenHeight);
      this.render();
    }

  }

  /**
   * Invoked before the component gets destroyed.
   * The renderer, controls, and the object's geometry and material will be disposed.
   */
  ngOnDestroy(): void {

    this.unsubscribe$.next();
    this.unsubscribe$.complete();

    this.disposeObject();

    this.controls.dispose();

    this.renderer.forceContextLoss();
    this.renderer.dispose();

  }

}
