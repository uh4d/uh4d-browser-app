import { Component, ElementRef, EventEmitter, forwardRef, Input, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractValueAccessor } from '@shared/utils/abstract-value-accessor';
import { IDate } from '@data/schema/image';

@Component({
  selector: 'uh4d-date-edit',
  templateUrl: './date-edit.component.html',
  styleUrls: ['./date-edit.component.sass'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => DateEditComponent),
    multi: true
  }]
})
export class DateEditComponent extends AbstractValueAccessor<IDate> {

  @Input() placeholder = 'unknown';
  @Input() enabled = true;
  @Output() saved = new EventEmitter<IDate>();

  @ViewChild('parentElement', {static: false}) inputParentElement: ElementRef;

  private preValue = '';

  editing = false;

  edit() {
    if (!this.value) {
      // @ts-ignore
      this.value = { value: '' };
    }
    this.preValue = this.value.value;
    this.editing = true;
    setTimeout(() => {
      this.inputParentElement.nativeElement.children[0].focus();
    });
  }

  save() {
    this.saved.emit(this.value);
    this.editing = false;
  }

  cancel() {
    this.value.value = this.preValue;
    this.editing = false;
  }

}
