import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { generate as generateId } from 'shortid';
import { TagData } from 'ngx-tagify';

@Component({
  selector: 'uh4d-tags-edit',
  templateUrl: './tags-edit.component.html',
  styleUrls: ['./tags-edit.component.sass'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TagsEditComponent),
    multi: true
  }]
})
export class TagsEditComponent implements ControlValueAccessor {

  @Input() placeholder = 'No tags';
  @Input() enabled = true;
  @Input() suggestionsFn: (query: string) => Observable<string[]>;
  @Output() saved = new EventEmitter<string[]>();

  private valueData: {value: string}[] = [];
  private preValue: {value: string}[] = [];
  private onChange: any = Function.prototype;
  private onTouched: any = Function.prototype;

  editing = false;
  name = generateId();
  suggestions$ = new Subject<TagData[]>();

  get value(): {value: string}[] {
    return this.valueData;
  }

  set value(v: {value: string}[]) {
    if (v !== this.valueData) {
      this.valueData = v;
      this.onChange(v.map(val => val.value));
    }
  }

  writeValue(value: string[]) {
    this.valueData = value ? value.map(v => ({value: v})) : [];
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  printValue() {
    return this.valueData.map(v => v.value).join(', ');
  }

  edit() {
    this.preValue = this.valueData;
    this.editing = true;
  }

  onInput(input: string) {
    if (this.suggestionsFn) {
      this.suggestionsFn(input)
        .subscribe(results => {
          this.suggestions$.next(results.map(t => ({value: t})));
        });
    }
  }

  save() {
    this.saved.emit(this.valueData.map(v => v.value));
    this.editing = false;
  }

  cancel() {
    this.value = this.preValue;
    this.editing = false;
  }

}
