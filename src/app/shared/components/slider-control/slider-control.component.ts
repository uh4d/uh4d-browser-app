import { Component, forwardRef, Input, OnInit, TemplateRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractValueAccessor } from '@shared/utils/abstract-value-accessor';
import { generate as generateId } from 'shortid';

@Component({
  selector: 'uh4d-slider-control',
  templateUrl: './slider-control.component.html',
  styleUrls: ['./slider-control.component.sass'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SliderControlComponent),
    multi: true
  }]
})
export class SliderControlComponent extends AbstractValueAccessor<number> implements OnInit {

  id = '';

  @Input() identifier: string;
  @Input() label: string;
  @Input() min = 0;
  @Input() max = 100;
  @Input() step = 5;
  @Input() left: TemplateRef<any>;
  @Input() right: TemplateRef<any>;
  @Input() type: 'inline' | 'default';

  ngOnInit(): void {
    this.id = this.identifier || 'id_' + generateId();
  }

}
