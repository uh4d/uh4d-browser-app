import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {QuickHelpItem, QuickHelpMap} from './quick-help-map';

@Component({
  selector: 'uh4d-quick-help',
  templateUrl: './quick-help.component.html',
  styleUrls: ['./quick-help.component.sass']
})
export class QuickHelpComponent implements OnInit {

  @Input() key: string;

  help: QuickHelpItem;
  content: string;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {

    if (!this.key) {
      throw new Error('No key specified for uh4d-quick-help');
    }

    this.help = QuickHelpMap.get(this.key);

  }

  onShown() {

    this.http.get(`assets/help/${this.help.html}.html`, {responseType: 'text'})
      .subscribe(html => {
        this.content = html;
      }, error => {
        console.error(error);
      });

  }

}
