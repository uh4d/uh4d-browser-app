import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, map, of, ReplaySubject, switchMap, take } from 'rxjs';
import { IImage } from '@data/schema/image';
import { IObject } from '@data/schema/object';
import { IPoi } from '@data/schema/poi';
import { ITerrain } from '@data/schema/terrain';
import { IMap } from '@data/schema/map';
import { ImageService } from '@data/services/image.service';
import { ObjectService } from '@data/services/object.service';
import { PoiService } from '@data/services/poi.service';
import { TerrainService } from '@data/services/terrain.service';
import { MapService } from '@data/services/map.service';

interface ISearchParams {
  q: string[];
  from: string;
  to: string;
  undated: string;
  model: string;
  map: string;
}

@Injectable({
  providedIn: 'root'
})
export class SearchManagerService {

  public images$ = new BehaviorSubject<IImage[]>([]);
  public models$ = new BehaviorSubject<IObject[]>([]);
  public map$ = new ReplaySubject<IMap>(1);
  public pois$ = new BehaviorSubject<IPoi[]>([]);
  public terrain$ = new ReplaySubject<string|ITerrain>(1);
  public maps$ = new ReplaySubject<IMap[]>(1);
  public dateExtent$ = new ReplaySubject<{ from: string, to: string }>(1);
  public osm$ = new ReplaySubject<any>(1);
  public osmAltitudes$ = new BehaviorSubject<{ [osmId: string]: number } | undefined>(undefined);

  public imagesLoading$ = new BehaviorSubject<boolean>(false);
  public modelsLoading$ = new BehaviorSubject<boolean>(false);

  private scene;

  private params: ISearchParams = {
    q: [],
    from: null,
    to: null,
    undated: null,
    model: null,
    map: null
  };

  public params$ = new BehaviorSubject<ISearchParams>(this.params);

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private imageService: ImageService,
    private objectService: ObjectService,
    private poiService: PoiService,
    private terrainService: TerrainService,
    private mapService: MapService
  ) {

    // listen to query parameters
    this.activatedRoute.queryParamMap.subscribe(params => {

      // image parameters
      let imageParamsUpdated = false;

      const qParams = params.getAll('q');
      if (qParams.length !== this.params.q.length || !qParams.every(value => this.params.q.includes(value))) {
        this.params.q = qParams;
        imageParamsUpdated = true;
      }

      if (params.get('from') !== this.params.from) {
        this.params.from = params.get('from');
        imageParamsUpdated = true;
      }
      if (params.get('to') !== this.params.to) {
        this.params.to = params.get('to');
        imageParamsUpdated = true;
      }
      if (params.get('undated') !== this.params.undated) {
        this.params.undated = params.get('undated');
        imageParamsUpdated = true;
      }

      // model parameters
      let modelParamsUpdated = false;

      if (params.get('model') !== this.params.model) {
        this.params.model = params.get('model');
        modelParamsUpdated = true;
      }

      // map parameters
      let mapParamsUpdated = false;

      if (params.get('map') !== this.params.map) {
        this.params.map = params.get('map');
        mapParamsUpdated = true;
      }

      if (imageParamsUpdated || modelParamsUpdated || mapParamsUpdated) {
        this.params$.next(this.params);
      }

      if (imageParamsUpdated) {
        this.queryImages();
      }

      if (modelParamsUpdated) {
        this.queryModels();
        this.queryPois();
      }

      if (mapParamsUpdated) {
        this.maps$
          .pipe(
            take(1),
            map(maps => maps.find(m => m.date === this.params.map))
          )
          .subscribe(mapData => {
            this.map$.next(mapData);
          });
      }

    });

  }

  setSceneData(data) {
    this.scene = data;
    this.queryTerrain();
    this.queryOSM();
    this.queryDateExtent();
  }

  queryImages() {
    if (this.isExploreRoute()) {
      this.imagesLoading$.next(true);
      this.imageService.query(Object.assign({
        lat: this.scene.latitude,
        lon: this.scene.longitude,
        r: 5000,
        // scene: this.globalRouteParams.get('scene')
      }, this.params))
        .subscribe(results => {
          this.imagesLoading$.next(false);
          this.images$.next(results);
        });
    } else {
      // omit if not in /explore route
    }
  }

  queryModels() {
    if (this.isExploreRoute()) {
      this.modelsLoading$.next(true);
      this.objectService.query({
        date: this.params.model,
        lat: this.scene.latitude,
        lon: this.scene.longitude,
        r: 5000,
        // scene: this.globalRouteParams.get('scene')
      })
        .subscribe(results => {
          this.modelsLoading$.next(false);
          this.models$.next(results);
        });
    } else {
      // omit if not in /explore route
    }
  }

  queryPois() {
    if (this.isExploreRoute()) {
      this.poiService.query({
        date: this.params.model,
        lat: this.scene.latitude,
        lon: this.scene.longitude,
        r: 5000,
        // scene: this.globalRouteParams.get('scene')
      })
        .subscribe(results => {
          this.pois$.next(results);
        });
    }
  }

  queryTerrain() {
    this.terrainService
      .query({
        lat: this.scene.latitude, lon: this.scene.longitude
      })
      .pipe(
        switchMap(results => {
          if (results[0]) {
            this.queryMaps(results[0].id);
            return of(results[0].plain());
          } else {
            this.maps$.next([]);
            return this.terrainService.requestElevationApi({
              lat: this.scene.latitude, lon: this.scene.longitude, r: 2500
            });
          }
        })
      )
      .subscribe((terrain) => {
        this.terrain$.next(terrain);
      });
  }

  queryMaps(terrainId: string) {
    this.mapService.query(terrainId)
      .subscribe(results => {
        this.maps$.next(results.plain());
      });
  }

  queryDateExtent() {
    this.imageService.getDateExtent({
      lat: this.scene.latitude, lon: this.scene.longitude, r: 5000
    })
      .subscribe(result => {
        this.dateExtent$.next(result);
      });
  }

  queryOSM() {
    this.terrainService.queryOSMData({
      lat: this.scene.latitude, lon: this.scene.longitude, r: 1000
    })
      .subscribe(result => {
        this.osm$.next(result);
      });
  }

  computeOSMAltitudes(terrainOrPath: ITerrain | string) {
    this.osmAltitudes$.next(undefined);

    let terrain = '';
    if (typeof terrainOrPath === 'string') {
      terrain = terrainOrPath;
    } else {
      terrain = terrainOrPath.id;
    }

    this.terrainService.computeOSMAltitudes({
      terrain,
      lat: this.scene.latitude,
      lon: this.scene.longitude,
      r: 1000
    })
      .subscribe(result => {
        this.osmAltitudes$.next(result);
      });
  }

  /**
   * Set query parameter for string search.
   */
  async setSearchParam(value: string[], add = false, remove = false) {
    let qParams = value;

    if (add) {
      qParams = this.params.q.concat(value);
    } else if (remove) {
      qParams = this.params.q.filter(q => !value.includes(q));
    }

    // remove duplicates
    qParams = [...new Set(qParams)];

    if (qParams.length === this.params.q.length && qParams.every(q => this.params.q.includes(q))) {
      return;
    }

    return this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        q: qParams
      },
      queryParamsHandling: 'merge'
    });
  }

  /**
   * Set query parameters for image dates.
   */
  async setImageDates(from: string, to: string, undated: boolean) {
    return this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        from, to, undated: undated ? '1' : '0'
      },
      queryParamsHandling: 'merge'
    });
  }

  /**
   * Set query parameter for model date.
   */
  async setModelDate(date: string) {
    return this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        model: date
      },
      queryParamsHandling: 'merge'
    });
  }

  /**
   * Set query parameter for map date.
   */
  async setMapDate(date: string) {
    return this.router.navigate([this.getExploreRoute()], {
      queryParams: {
        map: date
      },
      queryParamsHandling: 'merge'
    });
  }

  private isExploreRoute() {
    return /^\/explore/.test(this.router.url);
  }

  private getExploreRoute() {
    if (this.isExploreRoute()) {
      const index = this.router.url.indexOf('?');
      return index > -1 ? this.router.url.slice(0, index) : this.router.url;
    } else {
      return '/explore';
    }
  }

}
