import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { RestElement } from '@brakebein/ngx-restangular';
import { IImage } from '@data/schema/image';
import { IText } from '@data/schema/text';
import { IGradientConfig, VisualizationType } from '@data/schema/visualization';

type GlobalEvent = {
  key: 'spatialize',
  image: RestElement<IImage>
} | {
  key: 'visualization',
  type: VisualizationType
} | {
  key: 'gradient-update',
  config: IGradientConfig
} | {
  key: 'other'
};

@Injectable({
  providedIn: 'root'
})
export class GlobalEventsService {

  callAction$ = new Subject<GlobalEvent>();
  imageUpdate$ = new Subject<{image: IImage, prop?: string}>();
  textUpdate$ = new Subject<IText | void>();

  constructor() { }
}
