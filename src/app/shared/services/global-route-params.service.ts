import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalRouteParamsService {

  map = new Map<string, string>();

  constructor() { }

  get(paramId: string): string {
    return this.map.get(paramId);
  }

  set(paramId: string, value: string) {
    this.map.set(paramId, value);
  }

}
