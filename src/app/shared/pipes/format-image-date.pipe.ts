import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import { IDate } from '@data/schema/image';

@Pipe({
  name: 'formatImageDate'
})
export class FormatImageDatePipe implements PipeTransform {

  transform(value: IDate|string): string {

    if (typeof value === 'string') {
      return value;
    }

    if (!value || !value.display) {
      return 'unknown';
    }

    switch (value.display) {
      case 'YYYY/YYYY':
        return moment(value.from).format('YYYY') + '/' + moment(value.to).format('YYYY');
      case 'around YYYY':
        return 'around ' + moment(value.from).add(5, 'years').format('YYYY');
      case 'before YYYY':
        return 'before ' + moment(value.to).format('YYYY');
      case 'after YYYY':
        return 'after ' + moment(value.from).format('YYYY');
      default:
        return moment(value.from).format(value.display);
    }

  }

}
