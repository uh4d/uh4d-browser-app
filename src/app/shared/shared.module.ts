import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FaIconLibrary, FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { faMarkdown } from '@fortawesome/free-brands-svg-icons';
import { LineTruncationLibModule } from 'ngx-line-truncation';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TagifyModule } from 'ngx-tagify';
import { MarkdownModule, MarkedOptions } from 'ngx-markdown';
import { NgxPermissionsModule } from 'ngx-permissions';

import { markedOptionsFactory } from '@shared/marked-options-factory';
import { FormatImageDatePipe } from './pipes/format-image-date.pipe';
import { ImagePreviewDirective } from './directives/image-preview.directive';
import { FooterComponent } from '@shared/components/footer/footer.component';
import { InlineEditComponent } from './components/inline-edit/inline-edit.component';
import { TagsEditComponent } from './components/tags-edit/tags-edit.component';
import { TextareaEditComponent } from './components/textarea-edit/textarea-edit.component';
import { QuickHelpComponent } from './components/quick-help/quick-help.component';
import { SliderControlComponent } from './components/slider-control/slider-control.component';
import { DateEditComponent } from './components/date-edit/date-edit.component';
import { ImageViewerComponent } from './components/image-viewer/image-viewer.component';
import { ConfirmModalComponent } from './components/confirm-modal/confirm-modal.component';

import {
  faBuildingCustom,
  faFilterCustom,
  faFilterCustomSlash,
  faMapMarkerAltSlash,
  faMapMarkerAltSlashDashed,
  faMapMarkerLight, faScreenshot, faTourFlag, faTourFlagPlus,
  faCamSpatialSolid, faCamSpatialEmpty, faCamSpatialSlash, faCamSpatialFog
} from '@shared/icons';
import { MomentModule } from 'ngx-moment';


@NgModule({
  declarations: [
    FormatImageDatePipe,
    FooterComponent,
    InlineEditComponent,
    TagsEditComponent,
    TextareaEditComponent,
    QuickHelpComponent,
    SliderControlComponent,
    DateEditComponent,
    ImageViewerComponent,
    ConfirmModalComponent,
    ImagePreviewDirective
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    LineTruncationLibModule,
    ButtonsModule.forRoot(),
    PaginationModule.forRoot(),
    PopoverModule.forRoot(),
    ProgressbarModule.forRoot(),
    TypeaheadModule.forRoot(),
    ModalModule.forRoot(),
    TagifyModule.forRoot(),
    MarkdownModule.forRoot({
      markedOptions: {
        provide: MarkedOptions,
        useFactory: markedOptionsFactory
      }
    }),
    NgxPermissionsModule.forChild(),
    MomentModule
  ],
  exports: [
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    FormatImageDatePipe,
    FooterComponent,
    LineTruncationLibModule,
    ButtonsModule,
    PaginationModule,
    ProgressbarModule,
    TypeaheadModule,
    TagifyModule,
    MarkdownModule,
    NgxPermissionsModule,
    MomentModule,
    InlineEditComponent,
    TagsEditComponent,
    TextareaEditComponent,
    QuickHelpComponent,
    SliderControlComponent,
    DateEditComponent,
    ImageViewerComponent,
    ImagePreviewDirective
  ]
})
export class SharedModule {
  constructor(library: FaIconLibrary) {
    library.addIconPacks(fas, far);
    library.addIcons(
      faMarkdown,
      faMapMarkerLight, faMapMarkerAltSlash, faMapMarkerAltSlashDashed,
      faFilterCustom, faFilterCustomSlash,
      faBuildingCustom, faScreenshot,
      faTourFlag, faTourFlagPlus,
      faCamSpatialSolid, faCamSpatialEmpty, faCamSpatialSlash, faCamSpatialFog
    );
  }
}
