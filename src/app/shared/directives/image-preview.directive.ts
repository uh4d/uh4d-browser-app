import { Directive, ElementRef, Input } from '@angular/core';

/**
 * Pass {@link File} (e.g. from an upload form) to preview it as an image.
 */
@Directive({
  selector: 'img[uh4dPreview]'
})
export class ImagePreviewDirective {

  private fileValue: File;

  @Input()
  set uh4dPreview(file: File) {
    this.fileValue = file;
    this.readFile();
  }
  get uh4dPreview(): File {
    return this.fileValue;
  }

  constructor(
    private element: ElementRef<HTMLImageElement>
  ) { }

  private readFile(): void {

    if (this.fileValue) {

      const reader = new FileReader();

      reader.onloadend = () => {
        this.element.nativeElement.src = reader.result as string;
      };

      reader.readAsDataURL(this.fileValue);

    }

  }

}
