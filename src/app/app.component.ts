import { Component, HostListener, OnInit } from '@angular/core';
import { NgxRolesService } from 'ngx-permissions';

@Component({
  selector: 'uh4d-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  private permissions = [
    'editImages', 'manageImages', 'spatializeImages',
    'editObjects', 'manageObjects', 'managePOIs',
    'editTexts', 'manageTexts'
  ];

  constructor(
   private rolesService: NgxRolesService
  ) { }

  ngOnInit(): void {

    // restore edit mode
    if (localStorage.getItem('editMode')) {
      this.rolesService.addRoleWithPermissions('EDITOR', this.permissions);
    }

  }

  @HostListener('document:keyup.shift.alt.e')
  private toggleEditorMode() {

    const editMode = localStorage.getItem('editMode');

    if (editMode) {

      // leave edit mode, flush permissions
      this.rolesService.flushRolesAndPermissions();
      localStorage.removeItem('editMode');

    } else {

      // enter edit mode, add permissions
      this.rolesService.addRoleWithPermissions('EDITOR', this.permissions);
      localStorage.setItem('editMode', '1');

    }

  }

}
