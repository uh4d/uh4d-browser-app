import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { EMPTY, mergeMap, Observable, of } from 'rxjs';
import { IText } from '@data/schema/text';
import { TextService } from '@data/services/text.service';

/**
 * Resolver service invoked when entering {@link TextSheetComponent}.
 */
@Injectable({
  providedIn: 'root'
})
export class TextResolver implements Resolve<IText> {
  constructor(
    private router: Router,
    private textService: TextService
  ) {}

  /**
   * When entering the route, query database for text entry with given `textId` parameter.
   * If no entry found, navigate back to home page.
   */
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IText> {

    return this.textService.get(route.paramMap.get('textId'))
      .pipe(
        mergeMap((text) => {
          if (text.id) {
            return of(text);
          } else {
            // not found
            this.router.navigate(['/']);
            return EMPTY;
          }
        })
      );

  }
}
