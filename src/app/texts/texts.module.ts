import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';
import { PdfJsViewerModule } from 'ng2-pdfjs-viewer';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { SharedModule } from '@shared/shared.module';
import { TextUploadModalComponent } from './components/text-upload-modal/text-upload-modal.component';
import { TextListComponent } from './components/text-list/text-list.component';
import { TextSheetComponent } from './components/text-sheet/text-sheet.component';
import { CitationPipe } from './pipes/citation.pipe';
import { CitationEditComponent } from './components/citation-edit/citation-edit.component';
import { TextViewerComponent } from './components/text-viewer/text-viewer.component';
import { TextViewerAnnotationComponent } from './components/text-viewer-annotation/text-viewer-annotation.component';

@NgModule({
  declarations: [
    TextUploadModalComponent,
    TextListComponent,
    TextSheetComponent,
    CitationPipe,
    CitationEditComponent,
    TextViewerComponent,
    TextViewerAnnotationComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FileUploadModule,
    PdfJsViewerModule,
    TooltipModule,
  ],
  exports: [TextListComponent, TextUploadModalComponent]
})
export class TextsModule { }
