import { Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { RestElement } from '@brakebein/ngx-restangular';
import { NgxPermissionsService } from 'ngx-permissions';
import { IText } from '@data/schema/text';
import { GlobalEventsService } from '@shared/services/global-events.service';
import { ConfirmModalService } from '@shared/services/confirm-modal.service';

@Component({
  selector: 'uh4d-text-sheet',
  templateUrl: './text-sheet.component.html',
  styleUrls: ['./text-sheet.component.sass']
})
export class TextSheetComponent implements OnInit, OnDestroy {

  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Text database entry.
   */
  text: RestElement<IText>;

  /**
   * `true` if in edit mode, will unhide editable elements.
   */
  editable = true;

  /**
   * Flag that if `true` will unhide some buttons with more crucial operations.
   */
  expertMode = false;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly permissionsService: NgxPermissionsService,
    private readonly events: GlobalEventsService,
    private confirmService: ConfirmModalService,
  ) {}

  ngOnInit(): void {

    // get image data from resolver
    this.route.data.subscribe(data => {
      this.text = data.text;
      console.log(this.text);
    });

    // listen to permissions updates
    this.permissionsService.permissions$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(permissions => {
        this.editable = !!permissions.editTexts;
      });

  }

  /**
   * Update text reference information.
   */
  updateText(): void {
    this.text.patch<IText>({ bibtex: this.text.bibtex })
      .subscribe((updatedText) => {
        Object.assign(this.text, updatedText);
        this.events.textUpdate$.next(updatedText);
      });
  }

  /**
   * Remove text from database.
   * The user has to confirm this operation.
   * After successful removal, return to 3D viewport.
   */
  async removeText(): Promise<void> {
    const confirmed = await this.confirmService.confirm({
      message: 'This operation will remove the text from database and delete all corresponding files. Do you want to continue?'
    });

    if (!confirmed) { return; }

    this.text.remove()
      .subscribe(() => {
        this.router.navigate(['../..'], {
          relativeTo: this.route,
          queryParamsHandling: 'preserve'
        });
      });
  }

  /**
   * Open details sheet of associated object.
   */
  openObject(objectId: string): void {
    this.router.navigate([
      '/explore',
      this.route.snapshot.paramMap.get('scene'),
      'object',
      objectId
    ], {
      queryParamsHandling: 'preserve'
    });
  }

  /**
   * Toggle {@link #expertMode expertMode} when user presses <kbd>Shift</kbd> + <kbd>Alt</kbd> + <kbd>D</kbd>.
   * @private
   */
  @HostListener('document:keyup.shift.alt.d')
  private toggleExpertMode(): void {
    this.expertMode = !this.expertMode;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
