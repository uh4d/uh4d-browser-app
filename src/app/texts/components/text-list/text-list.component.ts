import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { lastValueFrom, Subject } from 'rxjs';
import { GlobalEventsService } from '@shared/services/global-events.service';
import { IText } from '@data/schema/text';
import { TextService } from '@data/services/text.service';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'uh4d-text-list',
  templateUrl: './text-list.component.html',
  styleUrls: ['./text-list.component.sass']
})
export class TextListComponent implements OnInit, OnDestroy {

  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  @Input() objectId: string;

  texts: IText[] = [];

  constructor(
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly textService: TextService,
    private readonly events: GlobalEventsService
  ) {}

  ngOnInit(): void {
    this.loadAssociatedTexts();

    this.events.textUpdate$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.loadAssociatedTexts();
      });
  }

  /**
   * Load texts associated filtered by object ID.
   */
  private async loadAssociatedTexts(): Promise<void> {
    this.texts = await lastValueFrom(
      this.textService.query({ q: [`obj:${this.objectId}`] })
    );
  }

  /**
   * Open text details sheet.
   */
  openText(textId: string): void {
    this.router.navigate([
      '/explore',
      this.route.snapshot.paramMap.get('scene'),
      'text',
      textId
    ], {
      queryParamsHandling: 'preserve'
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
