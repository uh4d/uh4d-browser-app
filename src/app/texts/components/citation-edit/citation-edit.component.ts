import { Component, ElementRef, EventEmitter, forwardRef, Input, OnInit, Output, ViewChild } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { AbstractValueAccessor } from '@shared/utils/abstract-value-accessor';

@Component({
  selector: 'uh4d-citation-edit',
  templateUrl: './citation-edit.component.html',
  styleUrls: ['./citation-edit.component.sass'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => CitationEditComponent),
    multi: true
  }]
})
export class CitationEditComponent extends AbstractValueAccessor<string> {

  @Input() placeholder = 'Empty';
  @Input() enabled = true;
  @Output() saved = new EventEmitter<string>();

  @ViewChild('inputElement', {static: false}) inputElement: ElementRef<HTMLTextAreaElement>;

  private preValue = '';
  editing = false;

  edit() {
    this.preValue = this.value;
    this.editing = true;
    setTimeout(() => {
      this.inputElement.nativeElement.focus();
    });
  }

  save() {
    this.saved.emit(this.value);
    this.editing = false;
  }

  cancel() {
    this.value = this.preValue;
    this.editing = false;
  }

}
