import { Component, Input, OnInit } from '@angular/core';
import { ITextFile } from '@data/schema/text';
import { AnnotationData } from '../text-viewer-annotation/text-viewer-annotation.component';

@Component({
  selector: 'uh4d-text-viewer',
  templateUrl: './text-viewer.component.html',
  styleUrls: ['./text-viewer.component.sass']
})
export class TextViewerComponent implements OnInit {

  @Input() file: ITextFile;

  showPdfFile = false;

  annotatedText: AnnotationData[];

  ngOnInit(): void {
    if (this.file.txt) { this.renderText(); }
    else if (this.file.pdf) { this.showPdfFile = true; }
  }

  private async renderText() {
    // load text
    const response = await fetch('/data/' + this.file.path + this.file.txt);
    const text = await response.text();

    let annotations: AnnotationData[] = [];

    if (this.file.json) {
      // load annotations
      const responseJson = await fetch('/data/' + this.file.path + this.file.json);
      annotations = await responseJson.json() as AnnotationData[];
      annotations.sort((a, b) => a.start - b.start);
    }

    // compose annotation data
    this.annotatedText = [];
    let textIndex = 0;

    annotations.forEach((ann) => {
      if (textIndex < ann.start) {
        this.annotatedText.push({
          text: text.slice(textIndex, ann.start),
          start: textIndex,
          end: ann.start,
          label: 'none'
        });
      }
      this.annotatedText.push({
        ...ann,
        text: text.slice(ann.start, ann.end)
      });
      textIndex = ann.end;
    });

    if (textIndex < text.length) {
      this.annotatedText.push({
        text: text.slice(textIndex),
        start: textIndex,
        end: text.length,
        label: 'none'
      });
    }
  }

}
