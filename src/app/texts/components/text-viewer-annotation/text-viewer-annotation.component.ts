import { Component, Input, OnInit } from '@angular/core';

export interface AnnotationData {
  text: string;
  start: number;
  end: number;
  label: string;
  wd_id?: string;
  aat_id?: string | number;
  ulan_id?: string | number;
}

@Component({
  selector: 'uh4d-text-viewer-annotation',
  templateUrl: './text-viewer-annotation.component.html',
  styleUrls: ['./text-viewer-annotation.component.sass']
})
export class TextViewerAnnotationComponent implements OnInit {

  @Input() data: AnnotationData;

  tooltipText: string;

  ngOnInit(): void {
    if (this.data.label !== 'none') {
      // set tooltip text
      this.tooltipText = `Label: ${this.data.label}`;
      if (this.data.wd_id) {
        this.tooltipText += `<br>Wikidata: ${this.data.wd_id}`;
      }
      if (this.data.aat_id) {
        this.tooltipText += `<br>AAT: ${this.data.aat_id}`;
      }
      if (this.data.ulan_id) {
        this.tooltipText += `<br>ULAN: ${this.data.ulan_id}`;
      }
    }
  }

}
