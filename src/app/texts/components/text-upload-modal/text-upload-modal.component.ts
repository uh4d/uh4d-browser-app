import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsService } from 'angular2-notifications';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { GlobalEventsService } from '@shared/services/global-events.service';

@Component({
  selector: 'uh4d-text-upload-modal',
  templateUrl: './text-upload-modal.component.html',
  styleUrls: ['./text-upload-modal.component.sass']
})
export class TextUploadModalComponent implements OnInit, OnDestroy {

  // config uploader
  uploader: FileUploader = new FileUploader({
    url: 'api/texts',
    itemAlias: 'uploadText',
    queueLimit: 1,
    filters: [{
      name: 'customFileType',
      fn: (file) => {
        const ext = file.name.split('.').pop();
        return ['pdf', 'zip'].includes(ext);
      }
    }]
  });

  fileItem: FileItem;

  hasFileOverDropZone = false;

  objectIds: string[] = [];

  constructor(
    private readonly elementRef: ElementRef<HTMLElement>,
    private readonly modalRef: BsModalRef,
    private readonly notify: NotificationsService,
    private readonly events: GlobalEventsService
  ) { }

  ngOnInit(): void {

    // listen to uploader events

    this.uploader.onWhenAddingFileFailed = (fileItem, filter) => {
      switch (filter.name) {
        case 'queueLimit':
          this.uploader.clearQueue();
          // @ts-ignore
          this.uploader.addToQueue([fileItem]);
          break;
        case 'fileType':
          this.notify.warn(`File format ${fileItem.type} not supported!`, 'Only common images are supported.');
          break;
        default:
          this.notify.error('Unknown error!', 'See console.');
      }

      console.warn('onWhenAddingFileFailed', fileItem, filter);
    };

    this.uploader.onAfterAddingFile = (fileItem) => {
      this.fileItem = fileItem;
    };

    this.uploader.onBuildItemForm = (fileItem, form: FormData) => {
      form.append('objects[0]', this.objectIds[0]);
    };

    this.uploader.onSuccessItem = (fileItem, response) => {
      this.notify.success('Upload successful!');
      this.events.textUpdate$.next(JSON.parse(response));
      this.close();
    };

    this.uploader.onErrorItem = (fileItem, response, status) => {
      this.notify.error('Error while uploading!');
      console.error('onErrorItem', fileItem, response, status);
    };

  }

  /**
   * Start upload process.
   */
  upload(): void {
    this.uploader.uploadAll();
  }

  /**
   * Open file dialog to select a file.
   */
  openFileDialog(): void {
    // trigger click event on hidden input element
    this.elementRef.nativeElement.querySelector<HTMLInputElement>('[ng2FileSelect]').click();
  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }

  ngOnDestroy(): void {
    this.uploader.destroy();
  }

}
