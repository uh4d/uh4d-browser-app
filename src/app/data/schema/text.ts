export interface IText {
  id: string;
  title: string;
  authors: string[];
  date: {
    value: string;
    from: string;
    to: string;
    display: string;
  };
  doi?: string;
  bibtex?: string;
  file: ITextFile;
  objects: { id: string, name: string }[];
  tags: string[];
}

export interface ITextFile {
  path: string;
  file: string;
  thumb: string;
  original: string;
  preview: string;
  type: string;
  pdf?: string;
  txt?: string;
  json?: string;
}
