import { ILocation } from '@data/schema/location';

export interface IMap {
  id: string;
  date: string;
  location: ILocation;
  file: {
    path: string;
  };
}
