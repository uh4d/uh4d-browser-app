import { HeatMap } from '@utils/viz/heat-map';
import { VectorField } from '@utils/viz/vector-field';
import { WindMap } from '@utils/viz/wind-map';
import { RadialFan } from '@utils/viz/radial-fan';

export type VisualizationType = 'heatmap' | 'vectorfield' | 'windmap' | 'radialfan' | 'none';

export type VisualizationInstance = HeatMap | VectorField | WindMap | RadialFan;

export interface IGradientConfig {
  gradient: { [key: string]: string };
  domain: [number, number];
}

export interface IVizMeta {
  label: string;
  description: string;
  imgSrc: string;
}

export const vizMeta = new Map<VisualizationType, IVizMeta>([
  ['heatmap', {
    label: 'Heat Map',
    description: 'Identify aggregations of images.',
    imgSrc: 'assets/viz/heatmap.jpg'
  }],
  ['vectorfield', {
    label: 'Vector Field',
    description: 'Identify aggregations of orientations of images.',
    imgSrc: 'assets/viz/vectorfield.jpg'
  }],
  ['windmap', {
    label: 'Particles',
    description: 'Identify aggregations of orientations of images. Similar to Vector Field, but fancier.',
    imgSrc: 'assets/viz/particles.jpg'
  }],
  ['radialfan', {
    label: 'Radial Fan',
    description: 'Identify aggregations of orientations of images for each cluster.',
    imgSrc: 'assets/viz/radialfan.jpg'
  }]
]);
