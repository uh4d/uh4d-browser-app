export interface ILocation {
  latitude: number;
  longitude: number;
  altitude: number;
}

export interface IOrientation {
  omega: number;
  phi: number;
  kappa: number;
}

export type ILocationOrientation = ILocation & IOrientation;
