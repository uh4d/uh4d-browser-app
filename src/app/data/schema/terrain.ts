import { ILocation } from '@data/schema/location';

export interface ITerrain {
  id: string;
  name: string;
  location: ILocation;
  file: {
    path: string;
    file: string;
    type: string;
  };
}
