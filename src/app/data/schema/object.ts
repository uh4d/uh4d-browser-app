import { ILocation, ILocationOrientation } from '@data/schema/location';

export interface IObject {
  id: string;
  name: string;
  date: {
    from: string;
    to: string;
  };
  origin: ILocationOrientation & {
    matrix: number[];
    scale?: number | number[];
  };
  file: IObjectFile;
  location: ILocation;
  misc: string;
  address: string;
  formerAddresses: string[];
  tags: string[];
  vrcity_forceTextures?: boolean;
}

export interface IObjectFile {
  path: string;
  file: string;
  original: string;
  type: string;
}

export interface IObjectTempFile extends IObjectFile {
  name: string;
}
