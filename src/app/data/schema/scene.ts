import { ILocation } from '@data/schema/location';

export interface IScene extends ILocation {
  id: string;
  name: string;
}
