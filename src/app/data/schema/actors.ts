export interface IAuthor {
  id: string;
  value: string;
}

export interface IOwner {
  id: string;
  value: string;
}
