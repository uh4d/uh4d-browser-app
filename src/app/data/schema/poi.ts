export interface IPoi {
  id: string;
  title: string;
  content: string;
  objectId: string;
  date: {
    from: string;
    to: string;
    objectBound: boolean;
  };
  location: {
    position: number[];
    longitude: number;
    latitude: number;
    altitude: number;
  };
  camera?: number[];
  timestamp?: string;
  damageFactors?: string[];
}

export interface ITour {
  id: string;
  title: string;
  pois: {
    id: string;
    title: string;
  }[];
}
