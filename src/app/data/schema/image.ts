import { Observable } from 'rxjs';
import { ILocationOrientation } from '@data/schema/location';

export interface IImage {
  id: string;
  title: string;
  description?: string;
  misc?: string;
  author?: string;
  owner?: string;
  permalink: string;
  captureNumber: string;
  date?: IDate;
  file: IImageFile;
  camera?: ICamera;
  spatialStatus: 0|1|2|3|4|5;
  tags: string[];
  spatialize?(postData: {[key: string]: any}): Observable<ICamera>;
}

export interface IImageFile {
  path: string;
  thumb: string;
  original: string;
  preview: string;
  tiny: string;
  type: string;
  width: number;
  height: number;
}

export interface IDate {
  from: string;
  to: string;
  value: string;
  display: string;
}

export interface ICamera extends ILocationOrientation {
  ck?: number;
  offset?: [number, number];
  matrix: number[];
}

export interface IImageUpdate {
  url?: string;
  type?: string;
  mime?: string;
  width?: number;
  height?: number;
  wUnits?: string;
  hUnits?: string;
  length?: number;
  updateAvailable: boolean;
  reason?: string;
}
