import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular, RestCollection } from '@brakebein/ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class TagService {

  constructor(
    private rest: Restangular,
  ) { }

  query(search?: string): Observable<RestCollection<{ value: string }>> {
    return this.rest
      .all<{ value: string }>('tags')
      .getList({search});
  }

}
