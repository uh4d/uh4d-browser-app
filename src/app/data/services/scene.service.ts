import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular } from '@brakebein/ngx-restangular';
import { IScene } from '@data/schema/scene';

@Injectable({
  providedIn: 'root'
})
export class SceneService {

  constructor(
    private rest: Restangular
  ) { }

  get(sceneId: string): Observable<IScene> {
    return this.rest
      .one<IScene>('scenes', sceneId)
      .get();
  }

}
