import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular, RestCollection, RestElement } from '@brakebein/ngx-restangular';
import { IPoi } from '@data/schema/poi';
import { ILocation } from '@data/schema/location';

@Injectable({
  providedIn: 'root'
})
export class PoiService {

  constructor(
    private rest: Restangular
  ) { }

  query(
    queryParams?: { date?: string, lat?: number, lon?: number, r?: number, scene?: string }
  ): Observable<RestCollection<IPoi>> {
    return this.rest
      .all<IPoi>('pois')
      .getList(queryParams);
  }

  get(id: string): Observable<RestElement<IPoi>> {
    return this.rest
      .one<IPoi>('pois', id)
      .get();
  }

  post(body: {
    title: string,
    content: string,
    location: ILocation,
    date: { from?: string, to?: string, objectBound: boolean },
    objectId?: string,
    camera?: number[],
    damageFactors?: string[]
  }): Observable<RestElement<IPoi>> {
    return this.rest
      .all('pois')
      .post<IPoi>(Object.assign({
        ...body,
        type: 'uh4d'
      }));
  }

}
