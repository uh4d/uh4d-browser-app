import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular, RestCollection, RestElement } from '@brakebein/ngx-restangular';
import { IAuthor } from '@data/schema/actors';

@Injectable({
  providedIn: 'root'
})
export class PersonService {

  constructor(
    private rest: Restangular
  ) { }

  query(search?: string): Observable<RestCollection<IAuthor>> {
    return this.rest
      .all<IAuthor>('persons')
      .getList(search ? {search} : undefined);
  }

  get(id: string): Observable<RestElement<IAuthor>> {
    return this.rest
      .one<IAuthor>('persons', id)
      .get();
  }

}
