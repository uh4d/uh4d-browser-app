import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular, RestCollection, RestElement } from '@brakebein/ngx-restangular';
import { IOwner } from '@data/schema/actors';

@Injectable({
  providedIn: 'root'
})
export class LegalBodyService {

  constructor(
    private rest: Restangular
  ) { }

  query(search?: string): Observable<RestCollection<IOwner>> {
    return this.rest
      .all<IOwner>('legalbodies')
      .getList(search ? {search} : undefined);
  }

  get(id: string): Observable<RestElement<IOwner>> {
    return this.rest
      .one<IOwner>('legalbodies', id)
      .get();
  }

}
