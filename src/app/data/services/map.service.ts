import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular, RestCollection } from '@brakebein/ngx-restangular';
import { IMap } from '@data/schema/map';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(
    private rest: Restangular
  ) { }

  query(
    terrainId: string,
    queryParams?: { date?: string }
  ): Observable<RestCollection<IMap>> {
    return this.rest
      .one('terrain', terrainId)
      .all<IMap>('maps')
      .getList(queryParams);
  }

}
