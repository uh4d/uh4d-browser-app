import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular, RestCollection, RestElement } from '@brakebein/ngx-restangular';
import { ITour } from '@data/schema/poi';

@Injectable({
  providedIn: 'root'
})
export class TourService {

  constructor(
    private rest: Restangular
  ) { }

  query(): Observable<RestCollection<ITour>> {
    return this.rest
      .all<ITour>('tours')
      .getList();
  }

  post(title: string, pois: string[]): Observable<RestElement<ITour>> {
    return this.rest
      .all('tours')
      .post<ITour>({
        title, pois
      });
  }

}
