import { Injectable } from '@angular/core';
import { Restangular, RestElement } from '@brakebein/ngx-restangular';
import { Observable } from 'rxjs';
import { DataUtilsService } from '@data/data-utils.service';
import { IObject, IObjectTempFile } from '@data/schema/object';
import { ILocationOrientation } from '@data/schema/location';

@Injectable({
  providedIn: 'root'
})
export class ObjectService {

  constructor(
    private rest: Restangular,
    private utils: DataUtilsService,
  ) { }

  query(
    queryParams?: { date?: string, lat?: number, lon?: number, r?: number, scene?: string }
  ): Observable<IObject[]> {
    return this.rest
      .all<IObject>('objects')
      .getList(queryParams)
      .pipe(this.utils.catchError());
  }

  get(id: string): Observable<RestElement<IObject>> {
    return this.rest
      .one<IObject>('objects', id)
      .get()
      .pipe(this.utils.catchError());
  }

  duplicate(obj: IObject): Observable<RestElement<IObject>> {
    return this.rest
      .one('objects', obj.id)
      .all('duplicate')
      .post<IObject>({ name: obj.name })
      .pipe(this.utils.catchError());
  }

  save(body: IObjectTempFile & ILocationOrientation & {matrix: number[]}): Observable<RestElement<IObject>> {
    return this.rest
      .all('objects')
      .post<IObject>(body)
      .pipe(this.utils.catchError());
  }

  removeUpload(id: string): Observable<any> {
    return this.rest
      .all('objects')
      .one('tmp', id)
      .remove()
      .pipe(this.utils.catchError());
  }

}
