import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Restangular, RestElement } from '@brakebein/ngx-restangular';
import { IImage, IImageFile, IImageUpdate } from '@data/schema/image';
import { DataUtilsService } from '../data-utils.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(
    private rest: Restangular,
    private utils: DataUtilsService,
  ) { }

  query(
    queryParams: { q?: string[], from?: string, to?: string, undated?: string, lat?: number, lon?: number, r?: number }
  ): Observable<IImage[]> {
    return this.rest
      .all<IImage>('images')
      .getList(queryParams)
      .pipe(this.utils.catchError());
  }

  get(id: string): Observable<IImage> {
    return this.rest
      .one<IImage>('images', id)
      .get()
      .pipe(
        tap<RestElement<IImage>>(result => {
          result.addRestangularMethod('spatialize', 'post', 'spatial');
        })
      );
      // .pipe(this.utils.catchError());
  }

  checkFileUpdate(id: string): Observable<IImageUpdate> {
    return this.rest
      .one('images', id)
      .one<IImageUpdate>('file/check')
      .get()
      .pipe(
        map(value => value.plain())
      );
  }

  updateFile(id: string): Observable<IImageFile> {
    return this.rest
      .one('images', id)
      .one<IImageFile>('file/update')
      .get()
      .pipe(
        map(value => value.plain())
      );
  }

  getDateExtent(
    queryParams: { lat?: number, lon?: number, r?: number }
  ): Observable<{ from: string, to: string }> {
    return this.rest
      .all('images')
      .one<{ from: string, to: string }>('dateExtent')
      .get(queryParams)
      .pipe(this.utils.catchError());
  }

}
