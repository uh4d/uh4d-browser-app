import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Restangular, RestCollection, RestElement } from '@brakebein/ngx-restangular';
import { ITerrain } from '@data/schema/terrain';

@Injectable({
  providedIn: 'root'
})
export class TerrainService {

  constructor(
    private rest: Restangular
  ) { }

  query(
    queryParams: { lat?: number, lon?: number }
  ): Observable<RestCollection<ITerrain>> {
    return this.rest
      .all<ITerrain>('terrain')
      .getList(queryParams);
  }

  get(id: string): Observable<RestElement<ITerrain>> {
    return this.rest
      .one<ITerrain>('terrain', id)
      .get();
  }

  requestElevationApi(queryParams: { lat: number, lon: number, r: number }): Observable<string> {
    return this.rest
      .one('elevation')
      .get({
        imageryProvider: 'MapBox-SatelliteStreet',
        textureQuality: 3,
        ...queryParams
      })
      .pipe(
        map(value => `api/elevation${(value as any).assetInfo.modelFile}`)
      );
  }

  queryOSMData(queryParams: { lat: number, lon: number, r: number }): Observable<any> {
    return this.rest
      .one('overpass')
      .get(queryParams);
  }

  computeOSMAltitudes(
    queryParams: { terrain: string, lat: number, lon: number, r: number }
  ): Observable<{ [osmId: string]: number }> {
    return this.rest
      .one<{ [osmId: string]: number }>('overpass/altitudes')
      .get(queryParams);
  }

}
