import { Injectable } from '@angular/core';
import { Restangular } from '@brakebein/ngx-restangular';
import { DataUtilsService } from '@data/data-utils.service';
import { Observable } from 'rxjs';
import { IText } from '@data/schema/text';

@Injectable({
  providedIn: 'root'
})
export class TextService {

  constructor(
    private rest: Restangular,
    private utils: DataUtilsService,
  ) {}

  query(queryParams: { q?: string[], from?: string, to?: string }): Observable<IText[]> {
    return this.rest
      .all<IText>('texts')
      .getList(queryParams)
      .pipe(this.utils.catchError());
  }

  get(id: string): Observable<IText> {
    return this.rest
      .one<IText>('texts', id)
      .get();
  }

}
