import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Restangular, RestCollection } from '@brakebein/ngx-restangular';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(
    private rest: Restangular
  ) { }

  query(search?: string): Observable<RestCollection<{ id: string, value: string }>> {
    return this.rest
      .all<{ id: string, value: string }>('addresses')
      .getList({search});
  }

}
