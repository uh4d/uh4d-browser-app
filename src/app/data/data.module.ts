import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestProvider, RestangularModule } from '@brakebein/ngx-restangular';


export function RestangularConfigFactory(RestangularProvider: RestProvider) {
  RestangularProvider.setBaseUrl('api');
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RestangularModule.forRoot(RestangularConfigFactory)
  ]
})
export class DataModule { }
