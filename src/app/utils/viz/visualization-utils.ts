import { Box2, Camera, Plane, Ray, Sphere, Vector2, Vector3 } from 'three';

export const defaultGradientConfig = {
  0.3: '#2b83ba', // blue
  0.5: '#abdda4', // cyan
  0.7: '#ffffbf', // green
  0.9: '#fdae61', // yellow
  1.0: '#d7191c'  // red
};

// export const defaultGradientConfig = {
//   0.0: '#3288bd',
//   0.1: '#66c2a5',
//   0.2: '#abdda4',
//   0.3: '#e6f598',
//   0.4: '#fee08b',
//   0.5: '#fdae61',
//   0.6: '#f46d43',
//   1.0: '#d53e4f'
// };

export const defaultPalette = getColorPalette(defaultGradientConfig);

export function getColorPalette(gradientConfig: { [key: string]: string }): Uint8ClampedArray {

  const paletteCanvas = document.createElement('canvas');
  const paletteCtx = paletteCanvas.getContext('2d');

  paletteCanvas.width = 256;
  paletteCanvas.height = 1;

  const gradient = paletteCtx.createLinearGradient(0, 0, 256, 1);
  for (const key of Object.keys(gradientConfig)) {
    gradient.addColorStop(parseFloat(key), gradientConfig[key]);
  }

  paletteCtx.fillStyle = gradient;
  paletteCtx.fillRect(0, 0, 256, 1);

  return paletteCtx.getImageData(0, 0, 256, 1).data;

}

/**
 * Compute rectangle that covers view frustum on ground plane.
 */
export function computeExtents(camera: Camera): {boundingBox: Box2, distance: number} {

  // ground plane and maximum distance sphere
  const plane = new Plane(new Vector3(0, 1, 0), 0);
  const maxSphere = new Sphere(camera.position, Math.max(camera.position.y, 1));

  // distance from camera to ground plane -> determine resolution
  const midRay = new Ray(
    camera.position,
    new Vector3(0, 0, 0.5).unproject(camera).sub(camera.position).normalize()
  );
  const midPoint = midRay.intersectPlane(plane, new Vector3()) || midRay.intersectSphere(maxSphere, new Vector3());
  const distance = midPoint.distanceTo(camera.position);

  // viewing frustum rays
  const rays = [
    new Ray(camera.position, new Vector3(-1, 1, 0.5)),
    new Ray(camera.position, new Vector3(-1, -1, 0.5)),
    new Ray(camera.position, new Vector3(1, -1, 0.5)),
    new Ray(camera.position, new Vector3(1, 1, 0.5))
  ];

  // determine bounding box around viewing quadrangle
  const boundingBox = new Box2();
  maxSphere.radius = distance * 1.5;

  rays.forEach(ray => {
    ray.direction.unproject(camera).sub(camera.position).normalize();
    const intersection = ray.intersectPlane(plane, new Vector3()) || ray.intersectSphere(maxSphere, new Vector3());
    boundingBox.expandByPoint(new Vector2(intersection.x, intersection.z));
  });

  return { boundingBox, distance };

}
