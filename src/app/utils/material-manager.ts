import {
  Color,
  DoubleSide,
  LineBasicMaterial,
  Material,
  MeshBasicMaterial,
  MeshLambertMaterial,
  MeshPhongMaterial,
  MeshPhysicalMaterial,
  MeshStandardMaterial, ShaderMaterial
} from 'three';
import { Settings } from '../viewport3d/viewport3d-settings';
import { XRayShader } from './xray-shader';

export type StandardMaterial = MeshBasicMaterial | MeshStandardMaterial | MeshLambertMaterial | MeshPhysicalMaterial | MeshPhongMaterial;

export class MaterialManager {

  private materials: Map<string, Material>;

  constructor() {

    this.materials = new Map();

    // STANDARD MATERIALS

    // default grey
    this.add(new MeshLambertMaterial({
      name: 'defaultMat',
      color: Settings.defaults.objectColor
    }), true);
    this.add(new MeshLambertMaterial({
      name: 'defaultDoubleSideMat',
      color: Settings.defaults.objectColor,
      side: DoubleSide
    }), true);
    this.add(new MeshLambertMaterial({
      name: 'defaultHighlightMat',
      color: new Color().lerp(new Color(Settings.defaults.highlightColor), 0.3)
    }), true);

    // transparent
    this.add(new MeshLambertMaterial({
      name: 'transparentMat',
      color: Settings.defaults.objectColor,
      transparent: true,
      opacity: 0.5
    }), true);
    this.add(new MeshLambertMaterial({
      name: 'transparentHighlightMat',
      color: new Color().lerp(new Color(Settings.defaults.highlightColor), 0.3),
      transparent: true,
      opacity: 0.5
    }), true);

    // xray
    this.add(new ShaderMaterial({
      name: 'xrayMat',
      side: DoubleSide,
      transparent: true,
      depthWrite: false,
      depthTest: false,
      uniforms: {
        ambient: { value: 0.1 },
        edgefalloff: { value: 0.1 },
        intensity: { value: 1.5 },
        vColor: { value: new Color(0x000000) },
      },
      vertexShader: XRayShader.vertexShader,
      fragmentShader: XRayShader.fragmentShader
    }), true);
    this.add(new ShaderMaterial({
      name: 'xraySelectionMat',
      side: DoubleSide,
      transparent: true,
      depthWrite: false,
      depthTest: false,
      uniforms: {
        ambient: {value: 0.1 },
        edgefalloff: { value: 0.3 },
        intensity: { value: 1.5 },
        vColor: { value: new Color(Settings.defaults.selectionColor) },
      },
      vertexShader: XRayShader.vertexShader,
      fragmentShader: XRayShader.fragmentShader
    }), true);
    this.add(new ShaderMaterial({
      name: 'xrayHighlightMat',
      side: DoubleSide,
      transparent: true,
      depthWrite: false,
      depthTest: false,
      uniforms: {
        ambient: { value: 0.1 },
        edgefalloff: { value: 0.3 },
        intensity: { value: 1.5 },
        vColor: { value: new Color(Settings.defaults.highlightColor) },
      },
      vertexShader: XRayShader.vertexShader,
      fragmentShader: XRayShader.fragmentShader
    }), true);

    // edges
    this.add(new LineBasicMaterial({
      name: 'edgesMat',
      color: Settings.defaults.edgeColor,
    }), true);
    this.add(new LineBasicMaterial({
      name: 'edgesSelectionMat',
      color: Settings.defaults.selectionColor
    }), true);

  }

  /**
   * Get material by id/name.
   */
  get(key: string): Material {
    return this.materials.get(key);
  }

  /**
   * Check if material instance is already added.
   */
  contains(material: Material): boolean {
    return [...this.materials.values()].includes(material);
  }

  /**
   * Add material to manager (if not already done, set mCount = 0 in userData).
   */
  private add(material: Material, persistent = false, prefix = ''): Material {
    if (!(material instanceof Material)) {
      console.warn('Material must bei subclass of THREE.Material!');
      return;
    }

    if (!material.name && !prefix) {
      console.warn('Material must have a name!');
    }

    // check if material is already part of manager
    const existingMat = this.get((prefix ? prefix + '_' : '') + material.userData.mName);

    if (existingMat) {
      if (!this.contains(material)) {
        this.disposeMaterial(material);
      }
      return existingMat;
    }

    // prepare metadata
    Object.assign(material.userData, {
      mCount: 0,
      mName: material.name,
      mPersistent: persistent
    });

    if (prefix) {
      material.name = prefix + '_' + material.name;
    }

    this.materials.set(material.name, material);

    return material;
  }

  /**
   * Add to manager if not already done and increment mCount
   */
  assign(key: string): Material;
  assign(material: Material, prefix?: string): Material;
  assign(keyOrMat: string|Material, prefix = ''): Material {
    let mat: Material;

    if (keyOrMat instanceof Material) {
      mat = this.add(keyOrMat, false, prefix);
    } else if (typeof keyOrMat === 'string') {
      mat = this.get(keyOrMat);
    } else {
      console.warn('Invalid key or material!');
      return;
    }

    if (!mat) {
      console.warn('Material not found!');
      return;
    }

    mat.userData.mCount++;

    return mat;
  }

  /**
   * Remove material(s) and dispose or decrement mCount
   */
  remove(materials: Material|Material[]) {
    if (!Array.isArray(materials)) {
      materials = [materials];
    }

    materials.forEach(material => {
      const mat = this.get(material.name);

      if (mat) {
        if (mat.userData.mCount > 1) {

          mat.userData.mCount--;

        } else if (!mat.userData.mPersistent) {

          this.materials.delete(mat.name);
          this.disposeMaterial(mat);

        }
      }
    });

    // TODO: set timeout, so materials are not instantly deleted
  }

  disposeMaterial(material: Material) {

    if (material.userData.mPersistent) {
      return;
    }

    if (material instanceof MeshBasicMaterial ||
      material instanceof MeshStandardMaterial ||
      material instanceof MeshLambertMaterial ||
      material instanceof MeshPhysicalMaterial ||
      material instanceof MeshPhongMaterial) {

      if (material.map) {
        material.map.dispose();
      }
      if (material.alphaMap) {
        material.alphaMap.dispose();
      }

    }
    material.dispose();
  }

}
