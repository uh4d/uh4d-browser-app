import { Vector3 } from 'three';
import { GenericItem } from './generic-item';
import { ImagePane } from '../image-pane';
import { IImage } from '@data/schema/image';

export class ImageItem extends GenericItem {

  object: ImagePane;
  scale = 1.0;
  source: IImage;
  hasOrientation = false;

  constructor(obj: ImagePane, label: string) {
    super(obj, label);
    this.source = obj.userData.source;
    this.hasOrientation = !!obj.fov;
  }

  /**
   * Set scale of item's object.
   */
  setScale(value: number) {
    this.scale = value;
    this.object.setScale(this.scale);
    this.update();
  }

  /**
   * Set opacity of the image pane.
   */
  setOpacity(value: number) {
    super.setOpacity(value);
    this.object.setOpacity(this.opacity);
    this.update();
  }

  /**
   * Activate/select the item and dispatch `select` event.
   * @param active - If not set, `active` property will be inverted.
   */
  select(active?: boolean) {
    super.select(active);

    if (this.active) {
      this.object.select();
    } else {
      this.object.deselect();
    }

    this.update();
  }

  /**
   * Highlight item.
   * @param highlighted - If not set, `highlighted` property will be inverted.
   */
  highlight(highlighted?: boolean) {
    if (this.active) { return; }

    super.highlight(highlighted);

    if (this.highlighted) {
      this.object.highlight();
    } else {
      if (!this.active) {
        this.object.dehighlight();
      }
    }

    this.update();
  }

  /**
   * Check if vector is within a certain distance and replace texture according as vector is within or outside threshold.
   * @param vector - Vector position, e.g. camera position.
   * @param distance - Threshold distance.
   */
  updateTexture(vector: Vector3, distance: number) {
    this.object.updateTexture(vector, distance)
      .then(updated => {
        if (updated) {
          this.update();
        }
      });
  }

  /**
   * Update orientation of image in order to look towards the given vector/camera position
   * (only applies to images without own orientation values).
   * @param vector - Vector position, e.g. camera position.
   */
  updateOrientation(vector: Vector3) {
    if (!this.hasOrientation) {
      this.object.lookAt(vector);
    }
  }

  /**
   * Remove any references to meshes and other items, so this item is ready for GC.
   * @param disposeObject - If true (default), object will get disposed too.
   */
  dispose(disposeObject: boolean = true) {
    if (disposeObject) {
      this.object.dispose();
    }
    super.dispose();
    delete this.source;
  }

}
