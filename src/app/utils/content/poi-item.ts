import { GenericItem } from './generic-item';
import { PointOfInterest } from '../point-of-interest';

export class PoiItem extends GenericItem {

  object: PointOfInterest;

  constructor(obj: PointOfInterest, label?: string) {

    super(obj, label);

  }

  highlight(highlighted?: boolean) {
    if (this.active) { return; }

    super.highlight(highlighted);
    this.object.highlight(this.highlighted);
  }

  select(active?: boolean) {
    super.select(active);
    this.object.select(this.active);
  }

  dispose() {

    this.object.dispose();
    super.dispose();

  }

}
