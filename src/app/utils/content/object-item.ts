/**
 * Extended GenericItem class for 3D objects.
 */
import { Color, LinearEncoding, LineSegments, Material, Mesh, MeshStandardMaterial, Object3D } from 'three';
import { GenericItem } from './generic-item';
import { Cache } from '../../viewport3d/viewport3d-cache';
import { Settings, ShadingKey } from '../../viewport3d/viewport3d-settings';
import { StandardMaterial } from '../material-manager';

export class ObjectItem extends GenericItem {

  edges: LineSegments;

  constructor(object: Object3D, label: string) {
    super(object, label);

    let count = 0;
    object.traverse(child => {
      if (child instanceof Mesh) {

        child.geometry.name = `geo_${count++}`;
        Cache.geometries.assign(child.geometry, object.name);

        const handleMaterial = (mat: Material): Material => {
          if ((mat as MeshStandardMaterial).map) {
            (mat as MeshStandardMaterial).map.encoding = LinearEncoding;
          }
          return Cache.materials.assign(mat, object.name);
        };

        if (Array.isArray(child.material)) {
          child.material = child.material.map(handleMaterial);
        } else {
          child.material = handleMaterial(child.material);
        }

        child.userData.originalMat = child.material;

      } else if (child instanceof LineSegments) {

        child.geometry.name = `geo_${count++}`;
        Cache.geometries.assign(child.geometry, object.name);

        // @ts-ignore
        child.material.dispose();
        child.material = Cache.materials.get('edgesMat');
        this.edges = child;

      }
    });

  }

  /**
   * Assign corresponding edges object.
   */
  setEdges(edges: LineSegments) {
    this.edges = edges;
  }

  /**
   * Activate/select the item.
   * @param active - If not set, `active` property will be inverted.
   */
  select(active?: boolean) {
    if (this.active === active) { return; }
    super.select(active);

    if (Settings.objects.shading === ShadingKey.XRay) {
      const mat = Cache.materials.get(this.highlighted ? 'xraySelectionMat' : 'xrayMat');
      this.object.traverse(child => {
        if (child instanceof Mesh) {
          child.material = mat;
        }
      });
    }

    if (this.edges) {
      this.edges.material = Cache.materials.get(this.active ? 'edgesSelectionMat' : 'edgesMat');
    }
  }

  /**
   * Highlight item.
   * @param highlighted - If not set, `highlighted` property will be inverted.
   */
  highlight(highlighted?: boolean) {
    if (this.highlighted === highlighted) { return; }
    super.highlight(highlighted);

    if (this.active && Settings.objects.shading === ShadingKey.XRay) {

      // no highlighting when active and xray shader
      return;

    } else if (Settings.objects.shading === ShadingKey.Color) {

      // copy original material and lerp color
      const hColor = new Color(Settings.defaults.highlightColor);
      const handleMaterial = (mat: Material): Material => {
        const hMat = mat.clone() as StandardMaterial;
        hMat.color.lerp(hColor, 0.3);
        return hMat;
      };

      this.object.traverse(child => {
        if (child instanceof Mesh) {
          if (this.highlighted) {
            if (Array.isArray(child.material)) {
              child.material = child.material.map(handleMaterial);
            } else {
              child.material = handleMaterial(child.material);
            }
          } else {
            if (child.material !== this.object.userData.originalMat) {
              (Array.isArray(child.material) ? child.material : [child.material]).forEach(mat => {
                mat.dispose();
              });
            }
            child.material = child.userData.originalMat;
          }
        }
      });

    } else {

      // apply predefined materials
      let mat: Material;

      switch (Settings.objects.shading) {
        case ShadingKey.Transparent:
          mat = Cache.materials.get(this.highlighted ? 'transparentHighlightMat' : 'transparentMat');
          break;
        case ShadingKey.XRay:
          mat = Cache.materials.get(this.highlighted ? 'xrayHighlightMat' : 'xrayMat');
          break;
        default:
          mat = Cache.materials.get(this.highlighted ? 'defaultHighlightMat' : 'defaultMat');
      }

      this.object.traverse(child => {
        if (child instanceof Mesh) {
          child.material = mat;
        }
      });

    }
  }

  /**
   * Apply shading/materials according to current viewport settings.
   */
  applyShading() {
    const hideEdges = !Settings.objects.showEdges || Settings.objects.shading === ShadingKey.XRay;

    if (hideEdges) {
      this.object.remove(this.edges);
    } else {
      this.object.add(this.edges);
    }

    if (Settings.objects.shading === ShadingKey.Color) {

      this.object.traverse(child => {
        if (child instanceof Mesh) {
          child.material = child.userData.originalMat;
        }
      });

    } else {

      let mat: Material;

      switch (Settings.objects.shading) {
        case ShadingKey.Transparent:
          mat = Cache.materials.get('transparentMat');
          break;
        case ShadingKey.XRay:
          mat = Cache.materials.get(this.active ? 'xraySelectionMat' : 'xrayMat');
          break;
        default:
          mat = Cache.materials.get('defaultMat');
      }

      this.object.traverse(obj => {
        if (obj instanceof Mesh) {
          obj.material = mat;
        }
      });
    }
  }

  /**
   * Remove any references to meshes and other items, so this item is ready for GC.
   * @param disposeObject - If true (default), object will get disposed too.
   */
  dispose(disposeObject: boolean = true) {

    if (disposeObject) {
      this.object.traverse(child => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          Cache.geometries.remove(child.geometry);
          Cache.materials.remove(child.material);
        }
      });
    }

    super.dispose();
    delete this.edges;

  }

}
