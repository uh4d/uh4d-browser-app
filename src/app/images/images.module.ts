import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '@shared/shared.module';
import { FileUploadModule } from 'ng2-file-upload';
import { ImageListComponent } from './components/image-list/image-list.component';
import { ImageSheetComponent } from './components/image-sheet/image-sheet.component';
import { ImageCollapseContainerComponent } from './components/image-collapse-container/image-collapse-container.component';
import { ImageUploadModalComponent } from './components/image-upload-modal/image-upload-modal.component';

/**
 * Module with resolver and (2D GUI) components related to image items.
 */
@NgModule({
  declarations: [
    ImageListComponent,
    ImageSheetComponent,
    ImageCollapseContainerComponent,
    ImageUploadModalComponent
  ],
  exports: [
    ImageListComponent,
    ImageCollapseContainerComponent
  ],
  imports: [
    CommonModule,
    FileUploadModule,
    SharedModule
  ]
})
export class ImagesModule { }
