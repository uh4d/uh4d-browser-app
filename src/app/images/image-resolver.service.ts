import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { catchError, mergeMap, take } from 'rxjs/operators';
import { IImage } from '@data/schema/image';
import { ImageService } from '@data/services/image.service';

/**
 * Resolver service invoked when entering {@link ImageSheetComponent}.
 */
@Injectable({
  providedIn: 'root'
})
export class ImageResolverService implements Resolve<IImage> {

  constructor(
    private router: Router,
    private imageService: ImageService
  ) { }

  /**
   * When entering the route, query database for image entry with given `imageId` parameter.
   * If no entry found, navigate back to home page.
   */
  resolve(route: ActivatedRouteSnapshot): Observable<IImage> | Promise<IImage> | IImage {

    return this.imageService.get(route.paramMap.get('imageId'))
      .pipe(
        take(1),
        catchError(err => {
          console.warn(err);
          let url = '';
          let parent = route.parent;
          while (parent) {
            if (parent.url[0]) {
              url = parent.url[0].path + '/' + url;
            }
            parent = parent.parent;
          }
          this.router.navigate([url]);
          return EMPTY;
        }),
        mergeMap(image => {
          if (image.id) {
            return of(image);
          } else {
            // not found
            this.router.navigate(['/']);
            return EMPTY;
          }
        })
      );

  }

}
