import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { BsModalService } from 'ngx-bootstrap/modal';
import { SearchManagerService } from '@shared/services/search-manager.service';
import { GlobalEventsService } from '@shared/services/global-events.service';
import { IImage } from '@data/schema/image';
import { Cache } from '../../../viewport3d/viewport3d-cache';
import { ImageUploadModalComponent } from '../image-upload-modal/image-upload-modal.component';

/**
 * Component that lists all queried image items.
 * The user can choose the list style, the number of entries per page, and the property to sort the entries.
 * A pagination lets users navigate through the list.
 *
 * ![Screenshot](../assets/image-list.png)
 */
@Component({
  selector: 'uh4d-image-list',
  templateUrl: './image-list.component.html',
  styleUrls: ['./image-list.component.sass']
})
export class ImageListComponent implements OnInit, OnDestroy {

  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Form group to control property to sort, items per page, and list style.
   */
  listForm: UntypedFormGroup = new UntypedFormGroup({
    sortBy: new UntypedFormControl('date'),
    sortDesc: new UntypedFormControl(false),
    itemsPerPage: new UntypedFormControl(20),
    listStyle: new UntypedFormControl('card')
  });

  /**
   * Image entries as received from the API.
   */
  imagesRaw: IImage[] = [];
  /**
   * Image entries sorted and filtered by {@link #listForm listForm} and pagination.
   */
  imagesSorted: IImage[] = [];

  /**
   * Current pagination page.
   */
  currentPage = 1;

  /**
   * Flag indicating if application is currently querying API for images.
   */
  isLoading = false;

  /**
   * Reference to container element of the list.
   */
  @ViewChild('listBodyElement', {static: true}) listBodyElement: ElementRef;

  constructor(
    private searchManager: SearchManagerService,
    private router: Router,
    private route: ActivatedRoute,
    private globalEvents: GlobalEventsService,
    private modalService: BsModalService
  ) { }

  /**
   * Invoked after the component is instantiated.
   * Listen to image query and form updates.
   */
  ngOnInit(): void {

    this.searchManager.images$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(images => {
        this.imagesRaw = images;
        this.sortImages();
      });

    this.searchManager.imagesLoading$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(loading => {
        this.isLoading = loading;
      });

    this.listForm.valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(() => {
        this.sortImages();
      });

    this.globalEvents.imageUpdate$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(event => {
        const image = this.imagesRaw.find(img => img.id === event.image.id);
        if (image) {
          image[event.prop] = event.image[event.prop];
        }
      });

  }

  /**
   * Sort {@link #imagesRaw imagesRaw} considering {@link #listForm listForm} and {@link #currentPage currentPage}.
   * @private
   */
  private sortImages(): void {

    const copy = this.imagesRaw.slice(0);

    // compare function
    const stringCompare = (a: string, b: string): number => {
      if (a && b) {
        return a.localeCompare(b);
      }
      if (a && !b) {
        return -1;
      }
      if (!a && b) {
        return 1;
      }
      return 0;
    };

    // function to resolve object properties
    const resolve = (path: string, obj) => {
      return path.split('.').reduce((prev, curr) => prev ? prev[curr] : null, obj);
    };

    // set compare order according to sortBy
    let compareOrder = [];
    switch (this.listForm.value.sortBy) {
      case 'author':
        compareOrder = ['author', 'date.from', 'title'];
        break;
      case 'title':
        compareOrder = ['title', 'date.from', 'author'];
        break;
      case 'date':
        compareOrder = ['date.from', 'author', 'title'];
    }

    // perform sorting
    copy.sort((a, b) => {
      for (const compareProp of compareOrder) {
        const result = stringCompare(resolve(compareProp, a), resolve(compareProp, b));
        if (result !== 0) {
          return result;
        }
      }
      return 0;
    });

    if (this.listForm.value.sortDesc) {
      copy.reverse();
    }

    // filter list according to itemsPerPage and currentPage
    this.imagesSorted = copy.slice(
      (this.currentPage - 1) * this.listForm.value.itemsPerPage,
      this.currentPage * this.listForm.value.itemsPerPage
    );

  }

  /**
   * Open image details sheet.
   */
  openImage(imageId: string): void {

    this.router.navigate(['./image', imageId], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve'
    });

  }

  /**
   * Return to /explore path and jump to image location.
   */
  zoomImage(event: MouseEvent, image: IImage): void {

    event.stopPropagation();
    if (!image.camera) { return; }

    const item = Cache.images.getByName(image.id);

    if (item) {
      this.router.navigate(['.'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve'
      });
      item.focus();
    }

  }

  /**
   * Pagination change event listener.
   * Scroll list container back to top and update {@link #imagesSorted imagesSorted}.
   */
  onPageChange(): void {

    this.listBodyElement.nativeElement.scrollTop = 0;

    setTimeout(() => {
      this.sortImages();
    });

  }

  /**
   * Open modal to upload images to the scene.
   */
  openUploadModal(): void {

    this.modalService.show(ImageUploadModalComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true
    });

  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {

    this.unsubscribe$.next();
    this.unsubscribe$.complete();

  }

}
