import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FileUploader } from 'ng2-file-upload';
import { NotificationsService } from 'angular2-notifications';
import { SearchManagerService } from '@shared/services/search-manager.service';
import { CoordConverterService } from '../../../viewport3d/services/coord-converter.service';

/**
 * Modal to upload a multiple images at once.
 */
@Component({
  selector: 'uh4d-image-upload-modal',
  templateUrl: './image-upload-modal.component.html',
  styleUrls: ['./image-upload-modal.component.sass']
})
export class ImageUploadModalComponent implements OnInit, OnDestroy {

  // config uploader
  uploader: FileUploader = new FileUploader({
    url: 'api/images',
    itemAlias: 'uploadImage',
    allowedFileType: ['image']
  });

  hasFileOverDropZone = false;

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private modalRef: BsModalRef,
    private notify: NotificationsService,
    private searchManager: SearchManagerService,
    private coordsConverter: CoordConverterService
  ) { }

  ngOnInit(): void {

    // listen to uploader events

    this.uploader.onWhenAddingFileFailed = (fileItem, filter) => {
      switch (filter.name) {
        case 'fileType':
          this.notify.warn(`File format ${fileItem.type} not supported!`, 'Only common images are supported.');
          break;
        default:
          this.notify.error('Unknown error!', 'See console.');
      }

      console.warn('onWhenAddingFileFailed', fileItem, filter);
    };

    this.uploader.onBuildItemForm = (fileItem, form) => {
      const origin = this.coordsConverter.getOrigin();
      form.append('latitude', origin.latitude);
      form.append('longitude', origin.longitude);
      // from this upload modal, we cannot define an exact location
      // hence big radius to define region
      form.append('radius', 10000);
    };

    this.uploader.onErrorItem = (fileItem, response, status) => {
      this.notify.error('Error while uploading!');
      console.error('onErrorItem', fileItem, response, status);
    };

    this.uploader.onCompleteAll = () => {
      this.searchManager.queryImages();
    };

  }

  /**
   * Start uploading all files.
   */
  uploadAll(): void {
    this.uploader.uploadAll();
  }

  /**
   * Open file dialog to select a file.
   */
  openFileDialog(): void {

    // trigger click event on hidden input element
    this.elementRef.nativeElement.querySelector<HTMLInputElement>('[ng2FileSelect]').click();

  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }

  ngOnDestroy(): void {
    this.uploader.destroy();
  }

}
