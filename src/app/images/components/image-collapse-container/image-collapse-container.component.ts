import { AfterViewInit, Component, ElementRef, NgZone, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SearchManagerService } from '@shared/services/search-manager.service';

/**
 * Container component that enables users to interactively resize the component's width.
 * The container can also be collapsed, so that only a small bar remains.
 * Additionally, the number of search results is displayed at the top.
 * The component wraps other content, such as {@link ImageListComponent}.
 *
 * ![In resize mode](../assets/image-collapse-container.png)&emsp;
 * ![In collapsed mode](../assets/image-collapse-container-bar.png)
 */
@Component({
  selector: 'uh4d-image-collapse-container',
  templateUrl: './image-collapse-container.component.html',
  styleUrls: ['./image-collapse-container.component.sass']
})
export class ImageCollapseContainerComponent implements AfterViewInit, OnInit, OnDestroy {

  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();
  /**
   * @ignore
   */
  private unlisten: (() => void)[] = [];
  /**
   * @ignore
   */
  private unlistenMouseDown: () => void;

  /**
   * Reference to the element that serves as interactive resize handle.
   */
  @ViewChild('resizeHandle', { static: false }) resizeHandle: ElementRef<HTMLDivElement>;

  /**
   * Initial size in pixels as set by the parent component.
   * @private
   */
  private initialSize = 0;

  /**
   * Offset from {@link #initialSize} in pixels.
   * @private
   */
  private resizeOffset = 0;

  /**
   * Current size in pixels in non-collapsed mode.
   * @private
   */
  private width = 600;

  /**
   * Flag indicating if the container is collapsed.
   */
  collapsed = false;

  /**
   * Number of search results displayed at the top.
   */
  imagesLength = 0;

  constructor(
    private searchManager: SearchManagerService,
    private renderer: Renderer2,
    private element: ElementRef<HTMLElement>,
    private ngZone: NgZone
  ) { }

  /**
   * Invoked after the component is instantiated.
   */
  ngOnInit(): void {

    this.renderer.setStyle(this.element.nativeElement, 'width', this.width + 'px');
    this.imagesLength = this.searchManager.images$.getValue().length;

  }

  /**
   * Invoked after the component's view is initialized.
   * Listen to image query updates and set initial width.
   */
  ngAfterViewInit(): void {

    // listen to images
    this.searchManager.images$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(images => {
        this.imagesLength = images.length;
      });

    // set initial width
    this.toggle(this.collapsed, false);

  }

  /**
   * Toggle collapse mode. Set width of container
   * and bind `mousedown` event to {@link #resizeHandle resizeHandle}.
   * @param collapse Set `true` to enter collapse mode, and vice versa
   * @param triggerEvent Set to `false` to prevent global `resize` event
   */
  toggle(collapse: boolean, triggerEvent: boolean = true): void {

    this.collapsed = collapse;
    this.renderer.setStyle(this.element.nativeElement, 'width', (this.collapsed ? 30 : this.width) + 'px');

    if (collapse) {

      if (this.unlistenMouseDown) {
        this.unlistenMouseDown();
        this.unlistenMouseDown = null;
      }

    } else {

      setTimeout(() => {
        this.ngZone.runOutsideAngular(() => {
          this.unlistenMouseDown = this.renderer.listen(this.resizeHandle.nativeElement, 'mousedown', this.onMouseDown.bind(this));
        });
      });

    }

    if (triggerEvent) {
      setTimeout(() => {
        window.dispatchEvent(new Event('resize'));
      });
    }

  }

  /**
   * `mousedown` event listener bound to {@link #resizeHandle resizeHandle}.
   * Bind `mousemove` and `mouseup` events.
   * @private
   */
  private onMouseDown(event: MouseEvent): void {

    event.preventDefault();

    if (event.button !== 0) { return; }

    this.resizeOffset = event.pageX;
    this.initialSize = this.element.nativeElement.offsetWidth;

    this.unlisten.push(
      this.renderer.listen(document, 'mousemove', this.onMouseMove.bind(this)),
      this.renderer.listen(document, 'mouseup', this.onMouseUp.bind(this))
    );

  }

  /**
   * `mousemove` event listener, update container width.
   * @private
   */
  private onMouseMove(event: MouseEvent): void {

    this.width = this.initialSize + this.resizeOffset - event.pageX;
    this.renderer.setStyle(this.element.nativeElement, 'width', Math.max(this.width, 400) + 'px');

  }

  /**
   * `mouseup` event listener, unbind `mousemove` and `mouseup` events.
   * Trigger global `resize` event if container width has changed.
   * @private
   */
  private onMouseUp(event: MouseEvent): void {

    this.unlisten.forEach(fn => (fn()));

    if (this.resizeOffset !== event.pageX) {
      // trigger resize
      window.dispatchEvent(new Event('resize'));
    }

  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {

    this.unsubscribe$.next();
    this.unsubscribe$.complete();

  }

}
