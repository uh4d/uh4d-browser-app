import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, of, Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { NotificationsService } from 'angular2-notifications';
import { RestElement } from '@brakebein/ngx-restangular';
import { NgxPermissionsService } from 'ngx-permissions';
import { GlobalEventsService } from '@shared/services/global-events.service';
import { PersonService } from '@data/services/person.service';
import { LegalBodyService } from '@data/services/legal-body.service';
import { TagService } from '@data/services/tag.service';
import { ImageService } from '@data/services/image.service';
import { IImage, IImageUpdate } from '@data/schema/image';
import { Cache } from '../../../viewport3d/viewport3d-cache';

/**
 * Component that shows all the metadata of an image.
 * In edit mode, the metadata can be updated.
 * The image is displayed within an interactive image viewer ({@link ImageViewerComponent}).
 *
 * ![Screenshot](../assets/image-sheet.png)
 */
@Component({
  selector: 'uh4d-image-sheet',
  templateUrl: './image-sheet.component.html',
  styleUrls: ['./image-sheet.component.sass']
})
export class ImageSheetComponent implements OnInit, OnDestroy {

  /**
   * @ignore
   */
  private unsubscribe$ = new Subject<void>();

  /**
   * Image database entry.
   */
  image: RestElement<IImage>;

  /**
   * Metadata about file update.
   */
  fileUpdate: IImageUpdate;

  /**
   * `true` if in edit mode, will unhide editable elements.
   */
  editable = true;

  /**
   * Will be `true` while updating the image file ({@link #updateFile}).
   */
  isUpdating = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private globalEvents: GlobalEventsService,
    private imageService: ImageService,
    private personService: PersonService,
    private legalBodyService: LegalBodyService,
    private tagService: TagService,
    private notify: NotificationsService,
    private permissionsService: NgxPermissionsService
  ) {

    this.queryAuthors = this.queryAuthors.bind(this);
    this.queryLegalBodies = this.queryLegalBodies.bind(this);
    this.queryTags = this.queryTags.bind(this);

  }

  /**
   * Invoked after the component is instantiated.
   * Get image data from resolver and listen to permission updates.
   */
  ngOnInit(): void {

    // get image data from resolver
    this.route.data.subscribe(data => {
      this.image = data.image;
      console.log(this.image);

      // check if an image update (bigger resolution) is available
      // only works for SLUB images
      // TODO: check only if in edit mode
      this.fileUpdate = null;
      this.imageService.checkFileUpdate(this.image.id)
        .subscribe({
          next: (result) => {
            console.log(result);
            this.fileUpdate = result;
          },
          error: () => {
            console.log('No update available.');
          }
        });

    });

    // listen to permissions updates
    this.permissionsService.permissions$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(permissions => {
        this.editable = !!permissions.editImages;
      });

  }

  /**
   * Return to 3D viewport and jump to the camera location.
   */
  zoomImage(): void {

    if (!this.image.camera) { return; }

    const item = Cache.images.getByName(this.image.id);

    if (item) {
      this.router.navigate(['../..'], {
        relativeTo: this.route,
        queryParamsHandling: 'preserve'
      });
      item.focus();
    }

  }

  /**
   * Return to 3D viewport and enter spatialization mode.
   */
  spatialize(): void {

    this.router.navigate(['../..'], {
      relativeTo: this.route,
      queryParamsHandling: 'preserve'
    });

    this.globalEvents.callAction$.next({
      key: 'spatialize',
      image: this.image
    });

  }

  /**
   * Update metadata of the image. Only one property can be updated at once.
   * @param prop Name of property that should be updated
   */
  updateImage(prop: string): void {

    switch (prop) {
      case 'title':
        // title property is required and can not be empty
        if (!this.image.title.length) {
          this.notify.error('Image must have a title!');
          return;
        }
        break;
    }

    // patch updates
    this.image.patch({ [prop]: this.image[prop] })
      .subscribe((result) => {
        console.log(result);
        if (prop === 'date') {
          this.image.date = result.date;
        }
        // signal image update to application
        this.globalEvents.imageUpdate$.next({
          image: this.image, prop
        });
      });

  }

  /**
   * Trigger an update of the image file (exchange file with new one).
   */
  updateFile(): void {

    this.isUpdating = true;

    this.imageService.updateFile(this.image.id)
      .subscribe({
        next: (result) => {
          this.image.file = result;
          this.globalEvents.imageUpdate$.next({
            image: this.image, prop: 'file'
          });
          this.fileUpdate = null;
          this.isUpdating = false;
        },
        error: () => {
          this.notify.error('File update failure!');
          this.isUpdating = false;
        }
      });

  }

  /**
   * Method passed to typeahead to query author names.
   * @param input Typed input
   */
  queryAuthors(input: string): Observable<string[]> {

    if (input) {
      return this.personService.query(input)
        .pipe(
          map(results => results.plain().map(r => r.value))
        );
    }

    return of([]);

  }

  /**
   * Method passed to typeahead to query name of owners/legal bodies.
   * @param input Typed input
   */
  queryLegalBodies(input: string): Observable<string[]> {

    if (input) {
      return this.legalBodyService.query(input)
        .pipe(
          map(results => results.plain().map(r => r.value))
        );
    }

    return of([]);

  }

  /**
   * Method passed to typeahead to query tags.
   * @param input Typed input
   */
  queryTags(input: string): Observable<string[]> {

    if (input) {
      return this.tagService.query(input)
        .pipe(
          map(results => results.plain().map(r => r.value))
        );
    }

    return of([]);

  }

  /**
   * Invoked before the component gets destroyed.
   */
  ngOnDestroy(): void {

    this.unsubscribe$.next();
    this.unsubscribe$.complete();

  }

}
