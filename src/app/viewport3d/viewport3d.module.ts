import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { SharedModule } from '@shared/shared.module';
import { Viewport3dComponent } from './viewport3d.component';
import { Canvas3dComponent } from './components/canvas3d/canvas3d.component';
import { CompassComponent } from './components/compass/compass.component';
import { LoadProgressComponent } from './components/load-progress/load-progress.component';
import { ViewportControlsComponent } from './components/viewport-controls/viewport-controls.component';
import { ViewportTooltipComponent } from './components/viewport-tooltip/viewport-tooltip.component';
import { ViewportContextMenuComponent } from './components/viewport-context-menu/viewport-context-menu.component';
import { IsolationControlsComponent } from './components/isolation-controls/isolation-controls.component';
import { SpatializeManualComponent } from './components/spatialize-manual/spatialize-manual.component';
import { PoiFormComponent } from './components/poi-form/poi-form.component';
import { ObjectUploadComponent } from './components/object-upload/object-upload.component';
import { TransformControlsComponent } from './components/transform-controls/transform-controls.component';
import { TourControlsComponent } from './components/tour-controls/tour-controls.component';
import { TourFormModalComponent } from './components/tour-form-modal/tour-form-modal.component';
import { TourManagerComponent } from './components/tour-manager/tour-manager.component';


@NgModule({
  declarations: [
    Viewport3dComponent,
    Canvas3dComponent,
    CompassComponent,
    LoadProgressComponent,
    ViewportControlsComponent,
    ViewportTooltipComponent,
    ViewportContextMenuComponent,
    IsolationControlsComponent,
    SpatializeManualComponent,
    PoiFormComponent,
    ObjectUploadComponent,
    TransformControlsComponent,
    TourControlsComponent,
    TourFormModalComponent,
    TourManagerComponent
  ],
  exports: [
    Viewport3dComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FileUploadModule,
    DragDropModule
  ]
})
export class Viewport3dModule { }
