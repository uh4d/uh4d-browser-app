import {
  AmbientLight,
  BackSide,
  Color,
  DirectionalLight,
  Mesh,
  Scene,
  ShaderMaterial,
  SphereBufferGeometry
} from 'three';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { Octree } from '@brakebein/threeoctree';
import { ClusterTree } from '@utils/cluster/cluster-tree';
import { GeometryManager } from '@utils/geometry-manager';
import { MaterialManager } from '@utils/material-manager';
import { Collection } from '@utils/content/collection';
import { ObjectItem } from '@utils/content/object-item';
import { ImageItem } from '@utils/content/image-item';
import { PoiItem } from '@utils/content/poi-item';
import { TerrainManager } from '@utils/terrain-manager';
import { LoadingManagerRx } from '@utils/loading-manager-rx';
import { Settings } from './viewport3d-settings';

// scene
const scene = new Scene();

// light
scene.add(new AmbientLight(0xffffff, 0.7));
const directionalLight = new DirectionalLight(0xffffff, 0.4);
directionalLight.position.set(-2, 8, 4);
scene.add(directionalLight);

// sky box
const skyGeo = new SphereBufferGeometry(4000, 32, 15);
const skyMat = new ShaderMaterial({
  uniforms: {
    topColor: { value: new Color().setHSL(0.6, 1, 0.6) },
    horizonColor: { value: new Color(0xffffff) },
    bottomColor: { value: new Color(0x666666) },
    offset: { value: 33 },
    topExponent: { value: 0.6 },
    bottomExponent: { value: 0.3 }
  },
  // language=GLSL
  vertexShader: 'varying vec3 vWorldPosition;\n\nvoid main() {\n\n\tvec4 worldPosition = modelMatrix * vec4(position, 1.0);\n\tvWorldPosition = worldPosition.xyz;\n\tgl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);\n\n}',
  // language=GLSL
  fragmentShader: 'uniform vec3 topColor;\nuniform vec3 horizonColor;\nuniform vec3 bottomColor;\nuniform float offset;\nuniform float topExponent;\nuniform float bottomExponent;\nvarying vec3 vWorldPosition;\n\nvoid main() {\n\n\tfloat h = normalize(vWorldPosition + offset).y;\n\tif (h > 0.0)\n\t\tgl_FragColor = vec4( mix( horizonColor, topColor, max( pow( h, topExponent ), 0.0 ) ), 1.0);\n\telse\n\t\tgl_FragColor = vec4( mix( horizonColor, bottomColor, max( pow( abs(h), bottomExponent ), 0.0 ) ), 1.0);\n\n}',
  side: BackSide
});
const skyMesh = new Mesh(skyGeo, skyMat);
skyMesh.renderOrder = -100;

scene.add(skyMesh);

// octree
const octree = new Octree();

// cluster tree
const clusterTree = new ClusterTree({
  scene, octree,
  distanceMultiplier: Settings.images.clusterDistance,
  scale: Settings.images.scale,
  opacity: Settings.images.opacity
});

// loading manager
const loadingManager = new LoadingManagerRx();

// gltf loader
const dracoLoader = new DRACOLoader();
dracoLoader.setDecoderPath('assets/gltf/');
dracoLoader.preload();
const gltfLoader = new GLTFLoader(loadingManager);
gltfLoader.setDRACOLoader(dracoLoader);

// terrain manager
const terrainManager = new TerrainManager(gltfLoader);
scene.add(terrainManager.get());

export const Cache = {

  scene,
  directionalLight,
  octree,
  clusterTree,

  viewpoint: {
    cameraPosition: Settings.defaults.viewpoint.cameraPosition.clone(),
    controlsTarget: Settings.defaults.viewpoint.controlsTarget.clone()
  },

  loadingManager,
  gltfLoader,

  terrainManager,

  geometries: new GeometryManager(),
  materials: new MaterialManager(),

  objects: new Collection<ObjectItem>(),
  images: new Collection<ImageItem>(),
  pois: new Collection<PoiItem>()

};
