import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MathUtils, PerspectiveCamera } from 'three';
import { NotificationsService } from 'angular2-notifications';
import * as moment from 'moment';
import { RestElement } from '@brakebein/ngx-restangular';
import { IImage } from '@data/schema/image';
import { ViewportEventsService } from '../../services/viewport-events.service';
import { CoordConverterService } from '../../services/coord-converter.service';

@Component({
  selector: 'uh4d-spatialize-manual',
  templateUrl: './spatialize-manual.component.html',
  styleUrls: ['./spatialize-manual.component.sass']
})
export class SpatializeManualComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();
  private startTime: string;

  image: RestElement<IImage>;
  camera: PerspectiveCamera;
  opacity = 50;
  precision = 1;
  controlMode = 'fly';
  editor = '';

  constructor(
    private events: ViewportEventsService,
    private notify: NotificationsService,
    private coordConverter: CoordConverterService
  ) { }

  ngOnInit(): void {
    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(action => {
        switch (action.key) {
          case 'spatialize-start':
            this.image = action.image;
            this.camera = action.camera;
            this.controlMode = 'fly';
            this.opacity = 50;
            this.precision = 1;
            this.editor = localStorage.getItem('metaEditor') || '';
            this.startTime = moment().format();
            this.setControls();
            break;
          case 'spatialize-end':
          case 'spatialize-abort':
            this.image = null;
            this.camera = null;
        }
      });
  }

  changeFOV() {
    this.camera.updateProjectionMatrix();
  }

  changePrecision() {
    this.camera.userData.movementFactor = this.precision;
    this.events.callAction$.next({key: 'controls-config', config: { rollSpeed: this.precision * 0.1 }});
  }

  setControls() {
    this.events.callAction$.next({key: 'controls-set', type: this.controlMode});
    if (this.controlMode === 'fly') {
      this.changePrecision();
    }
  }

  save() {
    if (!this.image || !this.camera) {
      return;
    }

    if (!this.editor) {
      this.notify.warn('You need to set a name or acronym!');
      return;
    }

    localStorage.setItem('metaEditor', this.editor);

    const location = this.coordConverter.toLatLon(this.camera.matrixWorld);

    this.image.camera = Object.assign(location, {
      matrix: this.camera.matrixWorld.toArray(),
      offset: [0, 0] as [number, number],
      ck: 1 / Math.tan((this.camera.fov / 2) * MathUtils.DEG2RAD) * 0.5
    });

    this.image.patch(null, { prop: 'camera' })
      .subscribe(() => {
        this.notify.success('Update successful!');
        this.events.callAction$.next({key: 'spatialize-end'});
      });
  }

  abort() {
    this.events.callAction$.next({key: 'spatialize-abort'});
  }

  blur(event: MouseEvent) {
    (event.target as HTMLElement).blur();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
