import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ViewportEventsService } from '../../services/viewport-events.service';
import { IPoi } from '@data/schema/poi';
import { TourService } from '@data/services/tour.service';

@Component({
  selector: 'uh4d-tour-form-modal',
  templateUrl: './tour-form-modal.component.html',
  styleUrls: ['./tour-form-modal.component.sass']
})
export class TourFormModalComponent implements OnInit {

  tourForm = new UntypedFormGroup({
    title: new UntypedFormControl('', Validators.required)
  });
  submitted = false;
  isSaving = false;

  pois: IPoi[] = [];

  constructor(
    private events: ViewportEventsService,
    private notify: NotificationsService,
    private modalRef: BsModalRef,
    private tourService: TourService
  ) { }

  ngOnInit(): void {

    this.pois = this.events.tourCollect$.getValue();

  }

  drop(event: CdkDragDrop<IPoi[]>) {
    moveItemInArray(this.pois, event.previousIndex, event.currentIndex);
  }

  removePoi(poi: IPoi) {

    const index = this.pois.findIndex(p => p.id === poi.id);
    if (index !== -1) {
      this.pois.splice(index, 1);
    }

  }

  save() {

    this.submitted = true;

    if (this.tourForm.invalid) {
      this.notify.warn('Form must not be empty!');
      return;
    }

    this.isSaving = true;

    this.tourService.post(this.tourForm.value.title, this.pois.map(p => p.id))
      .subscribe(result => {
        console.log(result);
        this.notify.success('Tour successfully saved!');
        this.events.tourCollect$.next(null);
        this.close();
      }, err => {
        console.error(err);
        this.notify.error('Error while saving!', 'See console for details.');
        this.isSaving = false;
      });

  }

  close() {
    this.modalRef.hide();
  }

}
