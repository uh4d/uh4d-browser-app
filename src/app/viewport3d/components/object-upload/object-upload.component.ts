import { Component, ElementRef, OnDestroy, OnInit } from '@angular/core';
import { FileItem, FileUploader } from 'ng2-file-upload';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsService } from 'angular2-notifications';
import { ViewportEventsService } from '../../services/viewport-events.service';

/**
 * Modal to select and upload an OBJ file (or zipped OBJ file with textures).
 * The file gets sent to the server, where it gets processed and converted
 * to a temporary stored .gltf file.
 *
 * ![Screenshot](../assets/viewport-object-upload-modal.png)
 */
@Component({
  selector: 'uh4d-object-upload',
  templateUrl: './object-upload.component.html',
  styleUrls: ['./object-upload.component.sass']
})
export class ObjectUploadComponent implements OnDestroy, OnInit {

  // config uploader
  // max 1 file of type .obj or .zip
  uploader: FileUploader = new FileUploader({
    url: `api/objects/tmp`,
    itemAlias: 'uploadModel',
    queueLimit: 1,
    filters: [{
      name: 'customFileType',
      fn: (file) => {
        const ext = file.name.split('.').pop();
        return ['obj', 'zip'].includes(ext);
      }
    }]
  });

  fileItem: FileItem;
  hasFileOverDropZone = false;

  constructor(
    private elementRef: ElementRef<HTMLElement>,
    private modalRef: BsModalRef,
    private notify: NotificationsService,
    private events: ViewportEventsService
  ) { }

  ngOnInit(): void {

    // listen to uploader events

    this.uploader.onWhenAddingFileFailed = (fileItem, filter) => {
      switch (filter.name) {
        case 'queueLimit':
          this.uploader.clearQueue();
          // @ts-ignore
          this.uploader.addToQueue([fileItem]);
          break;
        case 'customFileType':
          this.notify.warn('File format not supported!');
          break;
        default:
          this.notify.error('Unknown error!', 'See console.');
      }

      console.warn('onWhenAddingFileFailed', fileItem, filter);
    };

    this.uploader.onAfterAddingFile = (fileItem) => {
      this.fileItem = fileItem;
    };

    this.uploader.onSuccessItem = (fileItem, response) => {
      this.notify.success('Upload successful!');
      this.events.callAction$.next({
        key: 'object-temp',
        file: JSON.parse(response)
      });
      this.close();
    };

    this.uploader.onErrorItem = (fileItem, response, status) => {
      this.notify.error('Something went wrong!');
      console.error('onErrorItem', fileItem, response, status);
    };

  }

  /**
   * Start upload process.
   */
  upload(): void {
    this.uploader.uploadAll();
  }

  /**
   * Open file dialog to select a file.
   */
  openFileDialog(): void {

    // trigger click event on hidden input element
    this.elementRef.nativeElement.querySelector<HTMLInputElement>('[ng2FileSelect]').click();

  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }

  ngOnDestroy(): void {
    this.uploader.destroy();
  }

}
