import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { lastValueFrom } from 'rxjs';
import { NotificationsService } from 'angular2-notifications';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RestElement } from '@brakebein/ngx-restangular';
import { Vector3 } from 'three';
import { ViewportEventsService } from '../../services/viewport-events.service';
import { CoordConverterService } from '../../services/coord-converter.service';
import { SearchManagerService } from '@shared/services/search-manager.service';
import { ConfirmModalService } from '@shared/services/confirm-modal.service';
import { PoiService } from '@data/services/poi.service';
import { IPoi } from '@data/schema/poi';
import { ObjectService } from '@data/services/object.service';
import { IObject } from '@data/schema/object';
import { Cache } from '../../viewport3d-cache';

/**
 * This modal component has 3 tasks:
 * * display contents of Point of Interest (POI)
 * * form to edit title and content of POI
 * * form to save a new POI with title and content
 */
@Component({
  selector: 'uh4d-poi-form',
  templateUrl: './poi-form.component.html',
  styleUrls: ['./poi-form.component.sass']
})
export class PoiFormComponent implements OnInit {

  damageFactors = [
    { key: 'physical-forces', label: 'Physical forces' },
    { key: 'fire', label: 'Fire' },
    { key: 'vandalism', label: 'Theft + vandalism' },
    { key: 'dissociation', label: 'Dissociation' },
    { key: 'water', label: 'Water (flood / heavy rain)' },
    { key: 'light', label: 'Light + radiation' },
    { key: 'temperature', label: 'Harmful temperature' },
    { key: 'humidity', label: 'Harmful humidity' },
    { key: 'biological-attack', label: 'Biological attack' },
    { key: 'hazardous-substances', label: 'Hazardous substances' },
  ];

  title = 'Create Point of Interest';

  poiForm = new UntypedFormGroup({
    title: new UntypedFormControl('', Validators.required),
    content: new UntypedFormControl('', Validators.required),
    useObjectDate: new UntypedFormControl(false),
    from: new UntypedFormControl(''),
    to: new UntypedFormControl(''),
    factors: new UntypedFormArray(this.damageFactors.map(() => new UntypedFormControl(false)))
  });
  submitted = false;
  isEditing = false;
  isSaving = false;
  showPreview = false;

  object: IObject = null;
  position: Vector3 = new Vector3();

  poi: RestElement<IPoi>;
  content = '';
  iframeUrl: SafeResourceUrl = null;

  collectedPois: IPoi[]|null = null;

  constructor(
    private events: ViewportEventsService,
    private modalRef: BsModalRef,
    private notify: NotificationsService,
    private poiService: PoiService,
    private objectService: ObjectService,
    private searchManager: SearchManagerService,
    private confirmService: ConfirmModalService,
    private coordConverter: CoordConverterService,
    private sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {

    // set title and content if POI is passed (not creation mode)
    if (this.poi) {
      this.setTitle(this.poi.title);
      this.setContent(this.poi.content);
    }

    // collected POIs of new tour
    this.collectedPois = this.events.tourCollect$.getValue();
    // if this POI is already part of collection, do nothing
    if (this.collectedPois && !!this.collectedPois.find(cp => cp.id === this.poi.id)) {
      this.collectedPois = null;
    }

    // set form if object is available
    if (this.object) {
      this.poiForm.setValue({
        title: '',
        content: '',
        useObjectDate: true,
        from: this.object.date.from,
        to: this.object.date.to
      });
    }

    // reset dates if checkbox is toggled
    this.poiForm.controls.useObjectDate.valueChanges.subscribe(changes => {
      if (this.object && changes === true) {
        this.poiForm.controls.from.setValue(this.object.date.from);
        this.poiForm.controls.to.setValue(this.object.date.to);
      } else if (this.poi) {
        this.poiForm.controls.from.setValue(this.poi.date.from || '');
        this.poiForm.controls.to.setValue(this.poi.date.to || '');
      }
    });

  }

  /**
   * Set modal title depending on if in creation, editing, or display mode.
   * @private
   */
  private setTitle(title?: string): void {
    if (title) {
      this.title = title;
    } else {
      this.title = (this.isEditing ? 'Edit' : 'Create') + ' Point of Interest';
    }
  }

  /**
   * Set content. If it's an URL, than use sanitizer and prepare for iframe.
   * @private
   */
  private setContent(content?: string): void {

    this.content = '';
    this.iframeUrl = null;

    if (!content) { return; }

    // set url for iframe
    if (/^https?:\/\/\S+$/.test(content)) {
      if (/^https:\/\/\S+$/.test(content)) {
        // bypass secure link
        this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl(content);
      } else {
        // redirect insecure link
        this.iframeUrl = this.sanitizer.bypassSecurityTrustResourceUrl('api/redirect?url=' + encodeURIComponent(content));
      }
    } else {
      this.content = content;
    }

  }

  /**
   * Enter or leave editing mode. Set title and content respectively.
   */
  async toggleEdit(): Promise<void> {

    if (this.isEditing) {
      this.isEditing = false;
      this.showPreview = false;
      this.setTitle(this.poi.title);
      this.setContent(this.poi.content);
    } else {
      if (this.poi.objectId && !this.object) {
        this.object = await lastValueFrom(this.objectService.get(this.poi.objectId));
      }
      this.poiForm.setValue({
        title: this.poi.title,
        content: this.poi.content,
        useObjectDate: this.poi.date.objectBound,
        from: this.poi.date.from,
        to: this.poi.date.to,
        factors: this.damageFactors.map((value) => this.poi.damageFactors?.includes(value.key) || false)
      });
      this.isEditing = true;
      this.setTitle();
      this.setContent();
    }

  }

  /**
   * Toggle between form control and preview.
   */
  togglePreview(): void {

    if (this.showPreview) {
      this.setContent();
      this.showPreview = false;
    } else {
      this.setContent(this.poiForm.value.content);
      this.showPreview = true;
    }

  }

  /**
   * Submit form. Check if form is valid and either patch changes or save new POI.
   */
  save(): void {

    this.submitted = true;

    if (this.poiForm.invalid) {
      this.notify.warn('Form must not be empty!');
      return;
    }

    this.isSaving = true;

    const factors: string[] = this.poiForm.value.factors.map((value, i) => value && this.damageFactors[i].key).filter(value => value);

    if (this.poi) {

      // update existing poi
      this.poi.title = this.poiForm.value.title;
      this.poi.content = this.poiForm.value.content;
      this.poi.date.from = this.poiForm.value.from || null;
      this.poi.date.to = this.poiForm.value.to || null;
      this.poi.date.objectBound = this.poiForm.value.useObjectDate;
      this.poi.damageFactors = factors.length ? factors : null;

      this.poi.patch()
        .subscribe({
          next: (result) => {
            console.log(result);
            this.isSaving = false;
            this.toggleEdit();
            this.searchManager.queryPois();
          },
          error: (err) => {
            console.error(err);
            this.notify.error('Error while saving!', 'See console for details.');
            this.isSaving = false;
          }
        });

    } else {

      const location = this.coordConverter.toLatLon(this.position);

      // save new poi
      this.poiService.post( {
        title: this.poiForm.value.title,
        content: this.poiForm.value.content,
        location,
        date: {
          from: this.poiForm.value.from || undefined,
          to: this.poiForm.value.to || undefined,
          objectBound: this.poiForm.value.useObjectDate
        },
        objectId: this.object?.id,
        camera: [],
        damageFactors: factors,
      })
        .subscribe({
          next: (poi) => {
            console.log(poi);
            this.events.callAction$.next({key: 'poi-end'});
            this.searchManager.queryPois();
            this.close();
          },
          error: (err) => {
            console.error(err);
            this.notify.error('Error while saving!', 'See console for details.');
            this.isSaving = false;
          }
        });

    }

  }

  /**
   * Exit editing mode, or close modal if in creation mode.
   */
  cancel(): void {

    if (this.isEditing) {
      this.toggleEdit();
    } else {
      this.close();
    }

  }

  /**
   * Display confirm modal and call delete routine.
   */
  async remove(): Promise<void> {

    const confirmed = await this.confirmService.confirm({
      message: 'Do you really want to delete this Point of Interest?'
    });

    if (!confirmed) { return; }

    this.poi.remove()
      .subscribe(() => {
        this.searchManager.queryPois();
        this.close();
      });

  }

  /**
   * Add this POI to a new tour.
   */
  addToTour() {

    if (!this.collectedPois) { return; }

    this.collectedPois.push(this.poi);
    this.collectedPois = null;

  }

  /**
   * Jump to preferred camera position.
   */
  zoomPoi() {
    const item = Cache.pois.getByName(this.poi.id);
    item.focus();
    this.close();
  }

  /**
   * Close modal.
   */
  close(): void {
    this.modalRef.hide();
  }

}
