import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RestCollection, RestElement } from '@brakebein/ngx-restangular';
import { TourService } from '@data/services/tour.service';
import { ITour } from '@data/schema/poi';
import { ConfirmModalService } from '@shared/services/confirm-modal.service';

@Component({
  selector: 'uh4d-tour-manager',
  templateUrl: './tour-manager.component.html',
  styleUrls: ['./tour-manager.component.sass']
})
export class TourManagerComponent implements OnInit {

  tours: RestCollection<ITour>;

  constructor(
    private modalRef: BsModalRef,
    private tourService: TourService,
    private confirmService: ConfirmModalService
  ) { }

  ngOnInit(): void {

    this.tourService.query()
      .subscribe(results => {

        this.tours = results;

      });

  }

  async remove(tour: RestElement<ITour>): Promise<void> {

    const confirmed = await this.confirmService.confirm({
      message: 'Do you really want to delete this tour?'
    });

    if (!confirmed) { return; }

    tour.remove()
      .subscribe(() => {
        // remove from array
        const index = this.tours.indexOf(tour);
        if (index !== -1) {
          this.tours.splice(index, 1);
        }
      });

  }

  close(): void {
    this.modalRef.hide();
  }

}
