import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { RestElement } from '@brakebein/ngx-restangular';
import { Camera, Object3D, PerspectiveCamera, Vector2, Vector3 } from 'three';
import { GenericItem } from '@utils/content/generic-item';
import { ImageItem } from '@utils/content/image-item';
import { PoiItem } from '@utils/content/poi-item';
import { IImage } from '@data/schema/image';
import { IPoi } from '@data/schema/poi';
import { IObject, IObjectTempFile } from '@data/schema/object';


type ViewportActionEvent = {
  key: 'animate' | 'align-north' | 'home-view' | 'cluster-update' | 'shading-update' | 'isolation-exit' | 'isolation-zoom' | 'spatialize-abort' | 'spatialize-end' | 'poi-start' | 'poi-end' | 'screenshot'
} | {
  key: 'cluster-enable',
  enabled: boolean
} | {
  key: 'osm-enable',
  enabled: boolean
} | {
  key: 'controls-set',
  type: string
} | {
  key: 'controls-config',
  config: { [key: string]: any }
} | {
  key: 'isolation-start',
  item: ImageItem
} | {
  key: 'spatialize-start',
  image: RestElement<IImage>,
  camera: PerspectiveCamera
} | {
  key: 'poi-form',
  object: IObject | null,
  position: Vector3
} | {
  key: 'poi-open',
  poi: RestElement<IPoi>
} | {
  key: 'poi-camera',
  poi: PoiItem
} | {
  key: 'object-temp',
  file: IObjectTempFile
} | {
  key: 'transform-start',
  object: Object3D,
  mode: 'new' | 'update'
} | {
  key: 'transform-end',
  keepObject: boolean
} | {
  key: 'transform-update',
  object: IObject
};


@Injectable({
  providedIn: 'root'
})
export class ViewportEventsService {

  callAction$ = new Subject<ViewportActionEvent>();
  cameraUpdate$ = new Subject<Camera>();
  tooltip$ = new Subject<{item: GenericItem, position: Vector2} | void>();
  contextmenu$ = new Subject<{item: GenericItem, position: Vector2} | void>();

  tourCollect$ = new BehaviorSubject<IPoi[]|null>(null);

  constructor() { }
}
