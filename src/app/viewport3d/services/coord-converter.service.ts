import { Injectable } from '@angular/core';
import * as utm from 'utm';
import { Euler, Matrix4, Quaternion, Vector3 } from 'three';
import { ILocation, ILocationOrientation } from '@data/schema/location';

/**
 * Service used to convert coordinates/positions from one system to another (local Cartesian, WGS-84, UTM).
 */
@Injectable({
  providedIn: 'root'
})
export class CoordConverterService {

  /**
   * Origin of local coordinate system in UTM.
   */
  private origin = new Vector3();
  private latitude = 0;
  private longitude = 0;
  private zoneNum = 0;
  private zoneLetter = '';

  /**
   * Set origin of local coordinate system.
   */
  setOrigin(latitude: number, longitude: number, altitude: number): void {

    const { easting, northing, zoneNum, zoneLetter } = utm.fromLatLon(latitude, longitude);

    this.origin = new Vector3(easting, altitude, northing);
    this.latitude = latitude;
    this.longitude = longitude;
    this.zoneNum = zoneNum;
    this.zoneLetter = zoneLetter;

  }

  /**
   * Get origin in global WGS-84 coordinates.
   */
  getOrigin(): ILocation {
    return {
      latitude: this.latitude,
      longitude: this.longitude,
      altitude: this.origin.y
    };
  }

  /**
   * Convert local Cartesian position to global WGS-84 coordinates.
   */
  toLatLon(position: Vector3): ILocation;
  /**
   * Convert local transformation matrix to global WGS-84 coordinates and angles.
   */
  toLatLon(matrix: Matrix4): ILocationOrientation;
  toLatLon(vectorOrMatrix: Vector3|Matrix4): ILocation | ILocationOrientation {

    if (vectorOrMatrix instanceof Vector3) {

      const utmPosition = vectorOrMatrix.clone()
        .multiply(new Vector3(1, 1, -1))
        .add(this.origin);

      const { latitude, longitude } = utm.toLatLon(utmPosition.x, utmPosition.z, this.zoneNum, this.zoneLetter);

      return { latitude, longitude, altitude: utmPosition.y };

    } else {

      const utmPosition = new Vector3();
      const q = new Quaternion();
      vectorOrMatrix.decompose(utmPosition, q, new Vector3());
      utmPosition
        .multiply(new Vector3(1, 1, -1))
        .add(this.origin);
      const rotation = new Euler().setFromQuaternion(q, 'YXZ');

      const { latitude, longitude } = utm.toLatLon(utmPosition.x, utmPosition.z, this.zoneNum, this.zoneLetter);

      return { latitude, longitude, altitude: utmPosition.y, omega: rotation.x, phi: rotation.y, kappa: rotation.z };

    }

  }

  /**
   * Convert global WGS-84 coordinates to local Cartesian position.
   */
  fromLatLon(location: ILocation): Vector3;
  /**
   * Convert global WGS-84 coordinates to local Cartesian position.
   */
  fromLatLon(latitude: number, longitude: number, altitude: number): Vector3;
  fromLatLon(locationOrLatitude: ILocation | number, longitude?: number, altitude?: number): Vector3 {

    const location: ILocation = typeof locationOrLatitude === 'number'
      ? { latitude: locationOrLatitude, longitude: longitude || 0, altitude: altitude || 0 }
      : locationOrLatitude;

    const { easting, northing } = utm.fromLatLon(location.latitude, location.longitude, this.zoneNum);

    return new Vector3(easting, location.altitude, northing)
      .sub(this.origin)
      .multiply(new Vector3(1, 1, -1));

  }

  /**
   * Web Mercator (EPSG:3857) to WGS84 and local Cartesian.
   * https://alastaira.wordpress.com/2011/01/23/the-google-maps-bing-maps-spherical-mercator-projection/
   */
  fromWebMercator(x: number, y: number, altitude = 0): Vector3 {

    const longitude = (x / 20037508.34) * 180;
    const latitude = 180 / Math.PI * (2 * Math.atan(Math.exp((-y / 20037508.34) * Math.PI)) - Math.PI / 2);

    return this.fromLatLon(latitude, longitude, altitude);

  }

}
