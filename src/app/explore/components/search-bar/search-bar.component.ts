import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  NgZone,
  OnDestroy,
  OnInit
} from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NotificationsService } from 'angular2-notifications';
import { TagData, TagifyService, TagifySettings } from 'ngx-tagify';
import PerfectScrollbar from 'perfect-scrollbar';
import { SearchManagerService } from '@shared/services/search-manager.service';
import { PersonService } from '@data/services/person.service';
import { LegalBodyService } from '@data/services/legal-body.service';
import { ObjectService } from '@data/services/object.service';
import { regexpFilter, tagTemplate } from '../../../core/utils/tagify-templates';

@Component({
  selector: 'uh4d-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.sass']
})
export class SearchBarComponent implements AfterViewInit, OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();

  @Input() id = 'main';

  tagifySettings: TagifySettings = {
    placeholder: 'Start typing...',
    pattern: /^(?!\d{2,4}(?:[-.]\d{2,4})?(?:[-.]\d{2,4})?$).+$/,
    delimiters: '(?<!".*)(?:,| )+|(?<=".*")(?:,| )+',
    editTags: false,
    addTagOnBlur: false,
    templates: {
      tag: tagTemplate
    },
    callbacks: {
      invalid: () => {
        this.notify.info('Use the time slider to filter images by dates!');
      }
    }
  };

  private pScrollbar: PerfectScrollbar;

  private searchValuesData: TagData[] = [];

  get searchValues(): TagData[] {
    return this.searchValuesData;
  }

  set searchValues(value: TagData[]) {
    this.searchValuesData = value;
    this.updateSearch();
    this.pScrollbar.update();
  }

  constructor(
    private element: ElementRef,
    private searchManager: SearchManagerService,
    private tagifyService: TagifyService,
    private personService: PersonService,
    private legalBodyService: LegalBodyService,
    private objectService: ObjectService,
    private ngZone: NgZone,
    private notify: NotificationsService
  ) { }

  ngAfterViewInit(): void {
    // init tiny horizontal scrollbar
    const el = this.element.nativeElement.children[0];
    this.ngZone.runOutsideAngular(() => {
      this.pScrollbar = new PerfectScrollbar(el, {
        suppressScrollY: true,
        useBothWheelAxes: true
      });
    });
  }

  ngOnInit(): void {

    this.searchManager.params$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(params => {
        this.searchValuesData = params.q.map(v => {

          const loaded = this.searchValuesData.find(d => d.value === v);
          if (loaded) {
            return loaded;
          }

          const data: TagData = { value: v };

          const matches = regexpFilter.exec(v);

          if (!matches) {
            return data;
          }

          const [, type, id] = matches;
          data.type = type;

          switch (type) {
            case 'obj':
              this.objectService.get(id).subscribe(obj => {
                data.label = obj.name;
                this.updateTag(data);
              }, () => {
                data.label = id;
                this.updateTag(data);
              });
              break;
            case 'author':
              this.personService.get(id).subscribe(person => {
                data.label = person.value;
                this.updateTag(data);
              }, () => {
                data.label = id;
                this.updateTag(data);
              });
              break;
            case 'owner':
              this.legalBodyService.get(id).subscribe(legalBody => {
                data.label = legalBody.value;
                this.updateTag(data);
              }, () => {
                data.label = id;
                this.updateTag(data);
              });
          }

          return data;

        });
      });

  }

  private updateSearch(tags?: TagData[]): void {

    tags = tags ? tags : this.searchValuesData;

    this.searchManager.setSearchParam(tags.map(v => v.value));

  }

  private updateTag(data: TagData): void {

    const tagify = this.tagifyService.get(this.id + '-tagify');
    const tagElm = tagify.getTagElmByValue(data.value);
    // const tagElm = tagify.getTagElms().find(el => el.attributes.getNamedItem('value').textContent === data.value);
    tagify.replaceTag(tagElm, data);

  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
    this.pScrollbar.destroy();
  }

}
