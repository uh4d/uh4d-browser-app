import { Component, ElementRef, HostListener, OnInit } from '@angular/core';
import { TagifyService } from 'ngx-tagify';

@Component({
  selector: 'uh4d-explore-header',
  templateUrl: './explore-header.component.html',
  styleUrls: ['./explore-header.component.sass']
})
export class ExploreHeaderComponent implements OnInit {

  expanded = false;
  activeTab = '';

  constructor(
    private element: ElementRef<HTMLElement>,
    private tagifyService: TagifyService
  ) { }

  ngOnInit(): void {
  }

  expand(tab: string) {
    this.expanded = true;
    this.activeTab = tab;

    switch (tab) {
      case 'meta':
        setTimeout(() => {
          this.tagifyService.get('full-meta-tagify').DOM.input.focus();
        }, 50);
    }
  }

  collapse() {
    this.expanded = false;
    this.activeTab = '';
  }

  @HostListener('document:mousedown', ['$event'])
  private click(event: MouseEvent) {

    if (!(event.target instanceof Node && this.element.nativeElement.contains(event.target))) {
      this.collapse();
    }

  }

}
