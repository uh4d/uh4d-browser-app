import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GlobalEventsService } from '@shared/services/global-events.service';
import { IGradientConfig, IVizMeta, VisualizationType, vizMeta } from '@data/schema/visualization';

@Component({
  selector: 'uh4d-visualization-panel',
  templateUrl: './visualization-panel.component.html',
  styleUrls: ['./visualization-panel.component.sass']
})
export class VisualizationPanelComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();

  activeType: VisualizationType = 'none';
  activeViz: IVizMeta;
  availableViz: [VisualizationType, IVizMeta][] = [];
  gradientConfig: IGradientConfig;

  constructor(
    private events: GlobalEventsService
  ) { }

  ngOnInit(): void {

    this.availableViz = Array.from(vizMeta.entries());

    this.events.callAction$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(action => {
        switch (action.key) {

          case 'visualization':
            // set visualization
            this.activeType = action.type;
            this.activeViz = vizMeta.get(action.type);
            break;

          case 'gradient-update':
            this.gradientConfig = action.config;

        }
      });

  }

  select(type: VisualizationType): void {

    this.events.callAction$.next({
      key: 'visualization', type
    });

  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
