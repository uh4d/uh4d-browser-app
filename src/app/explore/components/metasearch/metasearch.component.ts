import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { SearchManagerService } from '@shared/services/search-manager.service';
import { IImage } from '@data/schema/image';
import { PersonService } from '@data/services/person.service';
import { LegalBodyService } from '@data/services/legal-body.service';
import { IAuthor, IOwner } from '@data/schema/actors';
import { IObject } from '@data/schema/object';

@Component({
  selector: 'uh4d-metasearch',
  templateUrl: './metasearch.component.html',
  styleUrls: ['./metasearch.component.sass']
})
export class MetasearchComponent implements OnInit, OnDestroy {

  private unsubscribe$ = new Subject<void>();

  authors: IAuthor[] = [];
  authorsFiltered: (IAuthor & {active: boolean})[] = [];
  owners: IOwner[] = [];
  ownersFiltered: (IOwner & {active: boolean})[] = [];
  objects: (IObject & {active: boolean})[] = [];

  constructor(
    private searchManager: SearchManagerService,
    private personsService: PersonService,
    private legalBodyService: LegalBodyService
  ) { }

  ngOnInit(): void {

    this.searchManager.images$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(images => {
        this.updateFacettedMeta(images);
      });

    this.searchManager.models$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(models => {
        this.updateFacettedObjects(models);
      });

  }

  private async updateFacettedMeta(images: IImage[]) {

    if (this.authors.length === 0) {
      this.authors = (await this.personsService.query().toPromise()).plain();
    }
    if (this.owners.length === 0) {
      this.owners = (await this.legalBodyService.query().toPromise()).plain();
    }

    // clear
    this.authorsFiltered = [];
    this.ownersFiltered = [];

    const qAuthor = this.searchManager.params$.getValue().q.filter(q => /^author:/.test(q));
    const qOwner = this.searchManager.params$.getValue().q.filter(q => /^owner:/.test(q));

    images.forEach(img => {

      if (img.author) {
        const index = this.authors.findIndex(a => a.value === img.author);
        if (index !== -1) {
          const author = this.authors[index];
          if (this.authorsFiltered.find(a => a.id === author.id)) { return; }
          this.authorsFiltered.push(Object.assign({
            active: qAuthor.some(q => q === 'author:' + author.id)
          }, author));
        }
      }

      if (img.owner) {
        const index = this.owners.findIndex(o => o.value === img.owner);
        if (index !== -1) {
          const owner = this.owners[index];
          if (this.ownersFiltered.find(a => a.id === owner.id)) { return; }
          this.ownersFiltered.push(Object.assign({
            active: qOwner.some(q => q === 'owner:' + owner.id)
          }, owner));
        }
      }

    });

    // sort
    this.authorsFiltered.sort((a, b) => a.value.localeCompare(b.value));
    this.ownersFiltered.sort((a, b) => a.value.localeCompare(b.value));

  }

  private updateFacettedObjects(objects: IObject[]) {

    const qObj = this.searchManager.params$.getValue().q.filter(q => /^obj:/.test(q));

    this.objects = objects.map(obj => Object.assign({active: qObj.some(q => q === 'obj:' + obj.id)}, obj));

    // sort
    this.objects.sort((a, b) => a.name.localeCompare(b.name));

  }

  filterAuthor(author: IAuthor & {active: boolean}) {
    this.searchManager.setSearchParam(['author:' + author.id], !author.active, author.active);
  }

  filterOwner(owner: IOwner & {active: boolean}) {
    this.searchManager.setSearchParam(['owner:' + owner.id], !owner.active, owner.active);
  }

  async filterObject(object: IObject & {active: boolean}) {
    await this.searchManager.setSearchParam(['obj:' + object.id], !object.active, object.active);
    object.active = !object.active;
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
