import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { ExploreRoutingModule } from './explore-routing.module';
import { SharedModule } from '@shared/shared.module';
import { Viewport3dModule } from '../viewport3d/viewport3d.module';
import { ImagesModule } from '../images/images.module';
import { ObjectsModule } from '../objects/objects.module';
import { TextsModule } from '../texts/texts.module';

import { ExploreComponent } from './explore.component';
import { ExploreHeaderComponent } from './components/explore-header/explore-header.component';
import { LegendGradientComponent } from './components/legend-gradient/legend-gradient.component';
import { MetasearchComponent } from './components/metasearch/metasearch.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { TimeSliderComponent } from './components/time-slider/time-slider.component';
import { VisualizationPanelComponent } from './components/visualization-panel/visualization-panel.component';
import { VisualizationPanelPreviewComponent } from './components/visualization-panel-preview/visualization-panel-preview.component';
import { MapSearchComponent } from './components/map-search/map-search.component';


@NgModule({
  declarations: [
    ExploreComponent,
    ExploreHeaderComponent,
    LegendGradientComponent,
    MetasearchComponent,
    SearchBarComponent,
    TimeSliderComponent,
    VisualizationPanelComponent,
    VisualizationPanelPreviewComponent,
    MapSearchComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    Viewport3dModule,
    ImagesModule,
    ObjectsModule,
    TextsModule,
    ExploreRoutingModule,
    LeafletModule
  ]
})
export class ExploreModule { }
