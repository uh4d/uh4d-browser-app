import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { EMPTY, Observable } from 'rxjs';
import { SearchManagerService } from '@shared/services/search-manager.service';
import { IScene } from '@data/schema/scene';
import { CoordConverterService } from '../viewport3d/services/coord-converter.service';

@Injectable({
  providedIn: 'root'
})
export class SceneResolverService implements Resolve<IScene> {

  constructor(
    private router: Router,
    private searchManager: SearchManagerService,
    private coordConverter: CoordConverterService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<IScene> | Promise<IScene> | IScene {

    const scene = route.paramMap.get('scene');

    // fallback for old scene links
    switch (scene) {
      case 'dresden':
        this.router.navigate(['/explore', '51.049329,13.738144']);
        return EMPTY;

      case 'jena':
        this.router.navigate(['/explore', '50.928172,11.587936']);
        return EMPTY;

      case 'amsterdam':
        this.router.navigate(['/explore', '52.371433,4.914622']);
        return EMPTY;

      default:
        const matches =
          /^(\(?[+-]?(?:90(?:\.0+)?|[1-8]?\d(?:\.\d+)?)),\s?([+-]?(?:180(?:\.0+)?|1[0-7]\d(?:\.\d+)?|\d{1,2}(?:\.\d+)?)\)?)$/.exec(scene);

        if (matches && matches[1] && matches[2]) {

          const location = {
            id: 'location',
            name: 'Location',
            latitude: parseFloat(matches[1]),
            longitude: parseFloat(matches[2]),
            altitude: 0
          };
          this.searchManager.setSceneData(location);
          this.coordConverter.setOrigin(location.latitude, location.longitude, location.altitude);
          return location;

        } else {

          this.router.navigate(['/explore']);
          return EMPTY;

        }
    }

  }

}
