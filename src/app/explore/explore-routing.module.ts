import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ExploreComponent } from './explore.component';
import { SceneResolverService } from './scene-resolver.service';
import { ImageSheetComponent } from '../images/components/image-sheet/image-sheet.component';
import { ImageResolverService } from '../images/image-resolver.service';
import { ObjectSheetComponent } from '../objects/components/object-sheet/object-sheet.component';
import { ObjectResolverService } from '../objects/object-resolver.service';
import { MapSearchComponent } from './components/map-search/map-search.component';
import { TextSheetComponent } from '../texts/components/text-sheet/text-sheet.component';
import { TextResolver } from '../texts/text.resolver';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: MapSearchComponent,
      },
      {
        path: ':scene',
        component: ExploreComponent,
        resolve: {
          scene: SceneResolverService
        },
        children: [
          {
            path: 'image/:imageId',
            component: ImageSheetComponent,
            resolve: {
              image: ImageResolverService
            }
          },
          {
            path: 'object/:objectId',
            component: ObjectSheetComponent,
            resolve: {
              object: ObjectResolverService
            }
          },
          {
            path: 'text/:textId',
            component: TextSheetComponent,
            resolve: {
              text: TextResolver,
            }
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExploreRoutingModule { }
