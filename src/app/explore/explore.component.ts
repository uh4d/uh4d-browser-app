import { Component, OnInit } from '@angular/core';
import { CoordConverterService } from '../viewport3d/services/coord-converter.service';
import { Cache } from '../viewport3d/viewport3d-cache';

@Component({
  selector: 'uh4d-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.sass']
})
export class ExploreComponent implements OnInit {

  constructor(
    private readonly coordsConverter: CoordConverterService
  ) { }

  ngOnInit(): void {
    // set coordinates converter
    Cache.terrainManager.setCoordsConverter(this.coordsConverter);
  }

}
