import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageWrapperComponent } from './core/components/page-wrapper/page-wrapper.component';
import { HomeComponent } from './core/pages/home/home.component';
import { HelpComponent } from './core/pages/help/help.component';
import { LegalnoticeComponent } from './core/pages/legalnotice/legalnotice.component';


const routes: Routes = [
  {
    path: '',
    component: PageWrapperComponent,
    children: [
      {
        path: '',
        component: HomeComponent,
        pathMatch: 'full'
      },
      {
        path: 'help',
        component: HelpComponent
      },
      {
        path: 'legalnotice',
        component: LegalnoticeComponent
      }
    ]
  },
  {
    path: 'explore',
    loadChildren: () => import('./explore/explore.module').then(m => m.ExploreModule)
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    paramsInheritanceStrategy: 'always'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
