import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { LegalnoticeComponent } from './pages/legalnotice/legalnotice.component';
import { HelpComponent } from './pages/help/help.component';
import { PageWrapperComponent } from './components/page-wrapper/page-wrapper.component';


@NgModule({
  declarations: [
    HeaderComponent,
    PageWrapperComponent,
    HomeComponent,
    LegalnoticeComponent,
    HelpComponent
  ],
  exports: [
    HeaderComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class CoreModule { }
